#pragma once
#include  "Compute_Parametrs.h"
#include  "DQE_Config.h"
#include  "FLR_P.h"
#include  "ImageProcessor.h"
#include "ProxyTransaction.h"
#include "ImageView.h"
namespace DQE {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form1
	/// </summary>
	public ref class MainView : public System::Windows::Forms::Form
	{
	public:
		DQE_Config*      _DQE_Config;
		ImageProcessor*  _ImageProcessor;
		FLR_P*           _FLR_P;
		Transaction* _DataForKernel;
		Compute_Parametrs^ _Compute_Parametrs;
		size_t _NPS_RowCounter;
		ImageView^ _NPS_View;
		ImageView^ _DQE_View;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  FileName;
	public:
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  UnitEnergy;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  �������;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  MTF_Axis;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  MTF_File;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  MTF_Area;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  MTF_Result;




	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Result;
	public:

	 virtual void WndProc(Message% m) override
	 {


		 switch (m.Msg)
		 {

		 case 0x10001:
		 {

			 if (!_DQE_Config->OpenConfig())
			 {
				 _DQE_Config->Close();
				 return;
			 }

			 uint16_t NPS_Unit = _DQE_Config->GetNPS_UnitDosa();
			 switch (NPS_Unit)
			 {
			 case NPS_DosaUnitType::NPS_DosaUnitType_mA:
			 {
				 UnitEnergy->HeaderText = "��";
				 break;
			 }


			 case NPS_DosaUnitType::NPS_DosaUnitType_mAs:
			 {
				 UnitEnergy->HeaderText = "��c";
				 break;
			 }

			 case NPS_DosaUnitType::NPS_DosaUnitType_mkGr:
			 {
				 UnitEnergy->HeaderText = "�r��";
				 break;
			 }
			 default:
				 break;
			 }
			


			 if (_DataForKernel->GetNPS_Data().size() != 0)
			 {




				 size_t AWidth = _DQE_Config->GetNPS_AreaEndX() - _DQE_Config->GetNPS_AreaBeginX();
				 size_t AHeight = _DQE_Config->GetNPS_AreaEndY() - _DQE_Config->GetNPS_AreaBeginY();
				 size_t W;
				 size_t H;

				 for (size_t i = 0; i <NPS_dataGridView->Rows->Count; i++)
				 {

					 W = _DataForKernel->GetNPS_Data()[i].FrameWidth;
					 H = _DataForKernel->GetNPS_Data()[i].FrameHeight;
					 
					 if (AWidth > W  && AHeight > H)
					 {
						 NPS_dataGridView->Rows[i]->Cells[2]->Value = "�������  ������� ���������  ������� ������";
						 NPS_dataGridView->Rows[i]->Cells[2]->Style->BackColor = System::Drawing::Color::Red;
					 }

					 if (AWidth <= W  && AHeight > H)
					 {
						 NPS_dataGridView->Rows[i]->Cells[2]->Value = "������  ������� ��������� ������ ������ ";
						 NPS_dataGridView->Rows[i]->Cells[2]->Style->BackColor = System::Drawing::Color::Red;
					 }


					 if (AWidth > W  && AHeight <= H)
					 {
						 NPS_dataGridView->Rows[i]->Cells[2]->Value = "������  ������� ��������� ������  ������ ";
						 NPS_dataGridView->Rows[i]->Cells[2]->Style->BackColor = System::Drawing::Color::Red;
					 }


					 if (AWidth < W  && AHeight < H)
					 {
						 NPS_dataGridView->Rows[i]->Cells[2]->Value = "��";
						 NPS_dataGridView->Rows[i]->Cells[2]->Style->BackColor = System::Drawing::Color::Green;

					 }

				 }
			 }


			 break;
		 }
		 }
		 

		 Form::WndProc(m);
	 }


		MainView(void)
		{
			InitializeComponent();
			 FrameLoadDialog->Filter = "RP Frames (*.flr)|*.flr";
			_Compute_Parametrs = gcnew Compute_Parametrs();
			_DQE_Config = new  DQE_Config();
			_ImageProcessor =  new  ImageProcessor();
			_FLR_P = new FLR_P();
			_NPS_RowCounter = 0;
			
			_NPS_View = gcnew ImageView();
			_DQE_View = gcnew ImageView ();

			_DataForKernel = new Transaction();


			for (size_t i = 0; i < 2; i++)
			{
				MTF_dataGridView->Rows->Add();

				if (i== 0)
				{
					MTF_dataGridView->Rows[0]->Cells[0]->Value = "X";//AXIS
				}
				if (i == 1)
				{
					MTF_dataGridView->Rows[1]->Cells[0]->Value = "Y";//AXIS
				}


				MTF_dataGridView->Rows[i]->Cells[1]->Value = "";//FILE
				MTF_dataGridView->Rows[i]->Cells[2]->Value = "";//AREA
				MTF_dataGridView->Rows[i]->Cells[2]->Value = "";//RESULT

			}


			if (_DQE_Config->OpenConfig())
			{

				uint16_t NPS_Unit = _DQE_Config->GetNPS_UnitDosa();
				switch (NPS_Unit)
				{
				case NPS_DosaUnitType::NPS_DosaUnitType_mA:
				{
					UnitEnergy->HeaderText = "��";
					break;
				}


				case NPS_DosaUnitType::NPS_DosaUnitType_mAs:
				{
					UnitEnergy->HeaderText = "��c";
					break;
				}

				case NPS_DosaUnitType::NPS_DosaUnitType_mkGr:
				{
					UnitEnergy->HeaderText = "�r��";
					break;
				}
				default:
					break;
				}
			}


			//
			//TODO: �������� ��� ������������
			//
		}
	private: System::Windows::Forms::OpenFileDialog^  FrameLoadDialog;
	public:
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn3;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  DQEx;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  DQEy;
	public: 

	protected:
		
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>

		~MainView()
		{

			if (_DataForKernel)
			{
				delete _DataForKernel;

			}



			if (_DQE_Config)
			{

				delete _DQE_Config;
			} 
		
			if (_ImageProcessor)
			{
				delete _ImageProcessor;
			}
			
			
			if (_FLR_P)
			{
				delete  _FLR_P;
			}

		
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip;
	protected:

	private: System::Windows::Forms::ToolStripMenuItem^  ParametrsCalculationMenuItem;
	protected: 
	private: System::Windows::Forms::TabControl^  tabControl1;

	private: System::Windows::Forms::TabPage^  tabPage2;
	private: System::Windows::Forms::TabPage^  tabPage1;


	private: System::Windows::Forms::TabPage^  tabPage3;
	private: System::Windows::Forms::StatusStrip^  statusStrip;

	private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel1;
private: System::Windows::Forms::DataGridView^  NPS_dataGridView;

	private: System::Windows::Forms::Button^  DQE_Compute;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chart1;
private: System::Windows::Forms::DataGridView^  MTF_dataGridView;



	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chart2;
private: System::Windows::Forms::DataGridView^  DQE_dataGridView;




	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chart3;
private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip;
private: System::Windows::Forms::ToolStripMenuItem^  OpenImage;
private: System::Windows::Forms::ToolStripMenuItem^  CleanItem;
private: System::Windows::Forms::ToolStripMenuItem^  CleeanTable;

	private: System::ComponentModel::IContainer^  components;


	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
	

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::Legend^  legend1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
			System::Windows::Forms::DataVisualization::Charting::Series^  series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea2 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::Legend^  legend2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
			System::Windows::Forms::DataVisualization::Charting::Series^  series2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle7 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle8 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle9 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea3 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::Legend^  legend3 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
			System::Windows::Forms::DataVisualization::Charting::Series^  series3 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			this->menuStrip = (gcnew System::Windows::Forms::MenuStrip());
			this->ParametrsCalculationMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->MTF_dataGridView = (gcnew System::Windows::Forms::DataGridView());
			this->contextMenuStrip = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->OpenImage = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->CleanItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->CleeanTable = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->chart1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->chart2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->NPS_dataGridView = (gcnew System::Windows::Forms::DataGridView());
			this->FileName = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->UnitEnergy = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->������� = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Result = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
			this->DQE_dataGridView = (gcnew System::Windows::Forms::DataGridView());
			this->dataGridViewTextBoxColumn3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->DQEx = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->DQEy = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->chart3 = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->DQE_Compute = (gcnew System::Windows::Forms::Button());
			this->statusStrip = (gcnew System::Windows::Forms::StatusStrip());
			this->toolStripStatusLabel1 = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->FrameLoadDialog = (gcnew System::Windows::Forms::OpenFileDialog());
			this->MTF_Axis = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->MTF_File = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->MTF_Area = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->MTF_Result = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->menuStrip->SuspendLayout();
			this->tabControl1->SuspendLayout();
			this->tabPage1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->MTF_dataGridView))->BeginInit();
			this->contextMenuStrip->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->BeginInit();
			this->tabPage2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->NPS_dataGridView))->BeginInit();
			this->tabPage3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DQE_dataGridView))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart3))->BeginInit();
			this->statusStrip->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip
			// 
			this->menuStrip->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->ParametrsCalculationMenuItem });
			this->menuStrip->Location = System::Drawing::Point(0, 0);
			this->menuStrip->Name = L"menuStrip";
			this->menuStrip->Size = System::Drawing::Size(1113, 24);
			this->menuStrip->TabIndex = 0;
			this->menuStrip->Text = L"menuStrip1";
			// 
			// ParametrsCalculationMenuItem
			// 
			this->ParametrsCalculationMenuItem->Name = L"ParametrsCalculationMenuItem";
			this->ParametrsCalculationMenuItem->Size = System::Drawing::Size(129, 20);
			this->ParametrsCalculationMenuItem->Text = L"��������� �������";
			this->ParametrsCalculationMenuItem->Click += gcnew System::EventHandler(this, &MainView::ParametrsCalculationMenuItem_Click);
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Controls->Add(this->tabPage3);
			this->tabControl1->Location = System::Drawing::Point(12, 27);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(1101, 543);
			this->tabControl1->TabIndex = 1;
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->MTF_dataGridView);
			this->tabPage1->Controls->Add(this->chart1);
			this->tabPage1->Controls->Add(this->button4);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(1093, 517);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"MTF";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// MTF_dataGridView
			// 
			this->MTF_dataGridView->AllowUserToAddRows = false;
			this->MTF_dataGridView->AllowUserToDeleteRows = false;
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle1->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle1->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->MTF_dataGridView->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this->MTF_dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->MTF_dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {
				this->MTF_Axis,
					this->MTF_File, this->MTF_Area, this->MTF_Result
			});
			this->MTF_dataGridView->ContextMenuStrip = this->contextMenuStrip;
			dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle2->BackColor = System::Drawing::SystemColors::Window;
			dataGridViewCellStyle2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle2->ForeColor = System::Drawing::SystemColors::ControlText;
			dataGridViewCellStyle2->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle2->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle2->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->MTF_dataGridView->DefaultCellStyle = dataGridViewCellStyle2;
			this->MTF_dataGridView->Location = System::Drawing::Point(6, 6);
			this->MTF_dataGridView->Name = L"MTF_dataGridView";
			dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle3->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle3->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle3->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle3->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle3->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->MTF_dataGridView->RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this->MTF_dataGridView->Size = System::Drawing::Size(448, 107);
			this->MTF_dataGridView->TabIndex = 4;
			this->MTF_dataGridView->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MainView::MTF_dataGridView_CellClick);
			// 
			// contextMenuStrip
			// 
			this->contextMenuStrip->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->OpenImage,
					this->CleanItem, this->CleeanTable
			});
			this->contextMenuStrip->Name = L"contextMenuStrip1";
			this->contextMenuStrip->Size = System::Drawing::Size(175, 70);
			// 
			// OpenImage
			// 
			this->OpenImage->Name = L"OpenImage";
			this->OpenImage->Size = System::Drawing::Size(174, 22);
			this->OpenImage->Text = L"�������";
			this->OpenImage->Click += gcnew System::EventHandler(this, &MainView::OpenImage_Click);
			// 
			// CleanItem
			// 
			this->CleanItem->Name = L"CleanItem";
			this->CleanItem->Size = System::Drawing::Size(174, 22);
			this->CleanItem->Text = L"�������";
			this->CleanItem->Click += gcnew System::EventHandler(this, &MainView::CleanItem_Click);
			// 
			// CleeanTable
			// 
			this->CleeanTable->Name = L"CleeanTable";
			this->CleeanTable->Size = System::Drawing::Size(174, 22);
			this->CleeanTable->Text = L"�������� �������";
			this->CleeanTable->Click += gcnew System::EventHandler(this, &MainView::CleeanTable_Click);
			// 
			// chart1
			// 
			chartArea1->Name = L"ChartArea1";
			this->chart1->ChartAreas->Add(chartArea1);
			legend1->Name = L"Legend1";
			this->chart1->Legends->Add(legend1);
			this->chart1->Location = System::Drawing::Point(659, 55);
			this->chart1->Name = L"chart1";
			series1->ChartArea = L"ChartArea1";
			series1->Legend = L"Legend1";
			series1->Name = L"Series1";
			this->chart1->Series->Add(series1);
			this->chart1->Size = System::Drawing::Size(353, 381);
			this->chart1->TabIndex = 5;
			this->chart1->Text = L"chart1";
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(6, 162);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(101, 23);
			this->button4->TabIndex = 3;
			this->button4->Text = L"���������";
			this->button4->UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->chart2);
			this->tabPage2->Controls->Add(this->button6);
			this->tabPage2->Controls->Add(this->NPS_dataGridView);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(1093, 517);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"NPS";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// chart2
			// 
			chartArea2->Name = L"ChartArea1";
			this->chart2->ChartAreas->Add(chartArea2);
			legend2->Name = L"Legend1";
			this->chart2->Legends->Add(legend2);
			this->chart2->Location = System::Drawing::Point(686, 62);
			this->chart2->Name = L"chart2";
			series2->ChartArea = L"ChartArea1";
			series2->Legend = L"Legend1";
			series2->Name = L"Series1";
			this->chart2->Series->Add(series2);
			this->chart2->Size = System::Drawing::Size(353, 419);
			this->chart2->TabIndex = 10;
			this->chart2->Text = L"chart2";
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(6, 162);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(75, 23);
			this->button6->TabIndex = 9;
			this->button6->Text = L"���������";
			this->button6->UseVisualStyleBackColor = true;
			// 
			// NPS_dataGridView
			// 
			this->NPS_dataGridView->AllowUserToAddRows = false;
			this->NPS_dataGridView->AllowUserToDeleteRows = false;
			dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle4->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle4->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle4->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle4->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle4->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->NPS_dataGridView->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
			this->NPS_dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->NPS_dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {
				this->FileName,
					this->UnitEnergy, this->�������, this->Result
			});
			this->NPS_dataGridView->ContextMenuStrip = this->contextMenuStrip;
			dataGridViewCellStyle5->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle5->BackColor = System::Drawing::SystemColors::Window;
			dataGridViewCellStyle5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle5->ForeColor = System::Drawing::SystemColors::ControlText;
			dataGridViewCellStyle5->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle5->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle5->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->NPS_dataGridView->DefaultCellStyle = dataGridViewCellStyle5;
			this->NPS_dataGridView->Location = System::Drawing::Point(6, 6);
			this->NPS_dataGridView->Name = L"NPS_dataGridView";
			dataGridViewCellStyle6->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle6->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle6->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle6->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle6->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle6->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->NPS_dataGridView->RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
			this->NPS_dataGridView->Size = System::Drawing::Size(446, 150);
			this->NPS_dataGridView->TabIndex = 1;
			this->NPS_dataGridView->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MainView::NPS_dataGridView_CellClick);
			// 
			// FileName
			// 
			this->FileName->HeaderText = L"����";
			this->FileName->Name = L"FileName";
			this->FileName->ReadOnly = true;
			this->FileName->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// UnitEnergy
			// 
			this->UnitEnergy->HeaderText = L"������� ���������";
			this->UnitEnergy->Name = L"UnitEnergy";
			this->UnitEnergy->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// �������
			// 
			this->�������->HeaderText = L"�������";
			this->�������->Name = L"�������";
			this->�������->ReadOnly = true;
			this->�������->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// Result
			// 
			this->Result->HeaderText = L"���������";
			this->Result->Name = L"Result";
			this->Result->ReadOnly = true;
			this->Result->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// tabPage3
			// 
			this->tabPage3->Controls->Add(this->DQE_dataGridView);
			this->tabPage3->Controls->Add(this->chart3);
			this->tabPage3->Controls->Add(this->DQE_Compute);
			this->tabPage3->Location = System::Drawing::Point(4, 22);
			this->tabPage3->Name = L"tabPage3";
			this->tabPage3->Size = System::Drawing::Size(1093, 517);
			this->tabPage3->TabIndex = 2;
			this->tabPage3->Text = L"DQE";
			this->tabPage3->UseVisualStyleBackColor = true;
			// 
			// DQE_dataGridView
			// 
			this->DQE_dataGridView->AllowUserToAddRows = false;
			this->DQE_dataGridView->AllowUserToDeleteRows = false;
			dataGridViewCellStyle7->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle7->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle7->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle7->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle7->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle7->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->DQE_dataGridView->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this->DQE_dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->DQE_dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
				this->dataGridViewTextBoxColumn3,
					this->DQEx, this->DQEy
			});
			dataGridViewCellStyle8->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle8->BackColor = System::Drawing::SystemColors::Window;
			dataGridViewCellStyle8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle8->ForeColor = System::Drawing::SystemColors::ControlText;
			dataGridViewCellStyle8->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle8->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle8->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->DQE_dataGridView->DefaultCellStyle = dataGridViewCellStyle8;
			this->DQE_dataGridView->Location = System::Drawing::Point(13, 12);
			this->DQE_dataGridView->MultiSelect = false;
			this->DQE_dataGridView->Name = L"DQE_dataGridView";
			dataGridViewCellStyle9->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle9->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle9->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle9->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle9->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle9->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->DQE_dataGridView->RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
			this->DQE_dataGridView->Size = System::Drawing::Size(341, 150);
			this->DQE_dataGridView->TabIndex = 10;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this->dataGridViewTextBoxColumn3->HeaderText = L"����";
			this->dataGridViewTextBoxColumn3->Name = L"dataGridViewTextBoxColumn3";
			// 
			// DQEx
			// 
			this->DQEx->HeaderText = L"DQEx";
			this->DQEx->Name = L"DQEx";
			// 
			// DQEy
			// 
			this->DQEy->HeaderText = L"DQEy";
			this->DQEy->Name = L"DQEy";
			// 
			// chart3
			// 
			chartArea3->Name = L"ChartArea1";
			this->chart3->ChartAreas->Add(chartArea3);
			legend3->Name = L"Legend1";
			this->chart3->Legends->Add(legend3);
			this->chart3->Location = System::Drawing::Point(732, 102);
			this->chart3->Name = L"chart3";
			series3->ChartArea = L"ChartArea1";
			series3->Legend = L"Legend1";
			series3->Name = L"Series1";
			this->chart3->Series->Add(series3);
			this->chart3->Size = System::Drawing::Size(353, 381);
			this->chart3->TabIndex = 9;
			this->chart3->Text = L"chart3";
			// 
			// DQE_Compute
			// 
			this->DQE_Compute->Location = System::Drawing::Point(13, 168);
			this->DQE_Compute->Name = L"DQE_Compute";
			this->DQE_Compute->Size = System::Drawing::Size(75, 23);
			this->DQE_Compute->TabIndex = 8;
			this->DQE_Compute->Text = L"���������";
			this->DQE_Compute->UseVisualStyleBackColor = true;
			// 
			// statusStrip
			// 
			this->statusStrip->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->toolStripStatusLabel1 });
			this->statusStrip->Location = System::Drawing::Point(0, 573);
			this->statusStrip->Name = L"statusStrip";
			this->statusStrip->Size = System::Drawing::Size(1113, 22);
			this->statusStrip->TabIndex = 2;
			this->statusStrip->Text = L"statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this->toolStripStatusLabel1->Name = L"toolStripStatusLabel1";
			this->toolStripStatusLabel1->Size = System::Drawing::Size(112, 17);
			this->toolStripStatusLabel1->Text = L"��������� �������";
			// 
			// MTF_Axis
			// 
			this->MTF_Axis->HeaderText = L"���";
			this->MTF_Axis->Name = L"MTF_Axis";
			this->MTF_Axis->ReadOnly = true;
			this->MTF_Axis->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// MTF_File
			// 
			this->MTF_File->HeaderText = L"����";
			this->MTF_File->Name = L"MTF_File";
			this->MTF_File->ReadOnly = true;
			this->MTF_File->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// MTF_Area
			// 
			this->MTF_Area->HeaderText = L"�������";
			this->MTF_Area->Name = L"MTF_Area";
			this->MTF_Area->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// MTF_Result
			// 
			this->MTF_Result->HeaderText = L"���������";
			this->MTF_Result->Name = L"MTF_Result";
			this->MTF_Result->ReadOnly = true;
			this->MTF_Result->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// MainView
			// 
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Inherit;
			this->ClientSize = System::Drawing::Size(1113, 595);
			this->Controls->Add(this->statusStrip);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->menuStrip);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MainMenuStrip = this->menuStrip;
			this->MaximizeBox = false;
			this->Name = L"MainView";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"DQE Calculator";
			this->menuStrip->ResumeLayout(false);
			this->menuStrip->PerformLayout();
			this->tabControl1->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->MTF_dataGridView))->EndInit();
			this->contextMenuStrip->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->EndInit();
			this->tabPage2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->NPS_dataGridView))->EndInit();
			this->tabPage3->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DQE_dataGridView))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart3))->EndInit();
			this->statusStrip->ResumeLayout(false);
			this->statusStrip->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void ParametrsCalculationMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

		if (!_Compute_Parametrs->Visible)
		{
			if (_Compute_Parametrs->IsDisposed)
			{
				_Compute_Parametrs = gcnew Compute_Parametrs();	
			}
			_Compute_Parametrs->MainViewHandle =(HWND)this->Handle.ToPointer();
			_Compute_Parametrs->Show();


		}
			 }
private: System::Void OpenImage_Click(System::Object^  sender, System::EventArgs^  e) {
	if (FrameLoadDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{

		_FLR_P->Open((char*)Marshal::StringToHGlobalAnsi(FrameLoadDialog->FileName).ToPointer());
		string  FileName = _FLR_P->Get_shortFileName();
		size_t W, H;
		_FLR_P->GetImageResolution(&W, &H);
	
		
		if (tabControl1->SelectedIndex == 0)///MTF
		{
			size_t Index = MTF_dataGridView->CurrentCell->RowIndex;
			MTF_dataGridView->Rows[MTF_dataGridView->CurrentCell->RowIndex]->Cells[1]->Value = gcnew String( FileName.c_str());
			_DataForKernel->PutMTFData(Index,(uint8_t*)FileName.c_str(), _FLR_P->GetDataBuffer(), W, H, _DQE_Config->GetMTF_WidthArea(), _DQE_Config->GetMTF_HeightArea(), _DQE_Config->GetMTF_ScaleArea());
		}


		if (tabControl1->SelectedIndex == 1)
		{
			NPS_dataGridView->Rows->Add();
			DQE_dataGridView->Rows->Add();
			size_t AWidth = _DQE_Config->GetNPS_AreaEndX() - _DQE_Config->GetNPS_AreaBeginX();
			size_t AHeight = _DQE_Config->GetNPS_AreaEndY() -  _DQE_Config->GetNPS_AreaBeginY();

			if (AWidth > W  && AHeight > H)
			{
				NPS_dataGridView->Rows[_NPS_RowCounter]->Cells[2]->Value = "�������  ������� ���������  ������� ������";
				NPS_dataGridView->Rows[_NPS_RowCounter]->Cells[2]->Style->BackColor = System::Drawing::Color::Red;
			}

			if (AWidth <= W  && AHeight > H)
			{
				NPS_dataGridView->Rows[_NPS_RowCounter]->Cells[2]->Value = "������  ������� ��������� ������ ������ ";
				NPS_dataGridView->Rows[_NPS_RowCounter]->Cells[2]->Style->BackColor = System::Drawing::Color::Red;
			}


			if (AWidth > W  && AHeight <= H)
			{
				NPS_dataGridView->Rows[_NPS_RowCounter]->Cells[2]->Value = "������  ������� ��������� ������  ������ ";
				NPS_dataGridView->Rows[_NPS_RowCounter]->Cells[2]->Style->BackColor = System::Drawing::Color::Red;
			}


			if (AWidth < W  && AHeight < H)
			{
				NPS_dataGridView->Rows[_NPS_RowCounter]->Cells[2]->Value = "��";
				NPS_dataGridView->Rows[_NPS_RowCounter]->Cells[2]->Style->BackColor = System::Drawing::Color::Green;
				
			}

		


			_DataForKernel->PutNPSData((uint8_t*)FileName.c_str(), _FLR_P->GetDataBuffer(), W, H, _DQE_Config->GetNPS_AreaBeginX(), _DQE_Config->GetNPS_AreaBeginY(), _DQE_Config->GetNPS_AreaEndX(), _DQE_Config->GetNPS_AreaEndY());
			NPS_dataGridView->Rows[_NPS_RowCounter]->Cells[0]->Value = gcnew String(FileName.c_str());
			DQE_dataGridView->Rows[_NPS_RowCounter]->Cells[0] ->Value = gcnew String(FileName.c_str());
			_NPS_RowCounter++;
		}

		_FLR_P->Close();
	}
}
private: System::Void CleeanTable_Click(System::Object^  sender, System::EventArgs^  e) {
	
	if (tabControl1->SelectedIndex == 0)///MTF
	{

		MTF_dataGridView->Rows->Clear();
		
		_DataForKernel->Clean_MTFData();
		for (size_t i = 0; i < 2; i++)
		{

			MTF_dataGridView->Rows->Add();

			if (i == 0)
			{
				MTF_dataGridView->Rows[0]->Cells[0]->Value = "X";//AXIS
			}
			if (i == 1)
			{
				MTF_dataGridView->Rows[1]->Cells[0]->Value = "Y";//AXIS
			}


			MTF_dataGridView->Rows[i]->Cells[1]->Value = "";//FILE
			MTF_dataGridView->Rows[i]->Cells[2]->Value = "";//AREA
			MTF_dataGridView->Rows[i]->Cells[3]->Value = "";//RESULT
			
			

		}

	}

	if (tabControl1->SelectedIndex == 1)
	{
		NPS_dataGridView->Rows->Clear();
		DQE_dataGridView->Rows->Clear();
		_DataForKernel->Clean_NPSData();
		_NPS_RowCounter = 0;
	}



}
private: System::Void CleanItem_Click(System::Object^  sender, System::EventArgs^  e) {

	if (tabControl1->SelectedIndex == 0)///MTF
	{
		size_t Index = MTF_dataGridView->CurrentCell->RowIndex;

		_DataForKernel->RemoveMTFData(Index);
		MTF_dataGridView->Rows[MTF_dataGridView->CurrentCell->RowIndex]->Cells[1]->Value = "";
		MTF_dataGridView->Rows[MTF_dataGridView->CurrentCell->RowIndex]->Cells[2]->Value = "";
		MTF_dataGridView->Rows[MTF_dataGridView->CurrentCell->RowIndex]->Cells[3]->Value = "";

		}


	if (tabControl1->SelectedIndex == 1)
	{

		if (NPS_dataGridView->Rows->Count > 0)
		{
			if (NPS_dataGridView->SelectedRows->Count == 1)
			{
				int RowIndex = NPS_dataGridView->CurrentCell->RowIndex;
				NPS_dataGridView->Rows->RemoveAt(RowIndex);
				_DataForKernel->RemoveNPSData(RowIndex);
				if (DQE_dataGridView->Rows->Count > 0)
				{
					DQE_dataGridView->Rows->RemoveAt(RowIndex);
				}
				
				_NPS_RowCounter--;
			}
			else
			{
				for each (DataGridViewRow^ Row in NPS_dataGridView->SelectedRows)
				{
					if (!Row->IsNewRow && Row->Index!=-1)
					{
						NPS_dataGridView->Rows->RemoveAt(Row->Index);
						DQE_dataGridView->Rows->RemoveAt(Row->Index);
						_NPS_RowCounter--;
					}
				}

			}

		}

	}


}

private: System::Void NPS_dataGridView_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {


	if (NPS_dataGridView->RowCount > 0)
	{

		int RowIndex = NPS_dataGridView->CurrentCell->RowIndex;
		int ColumIndex = NPS_dataGridView->CurrentCell->ColumnIndex;
		String^ Caption;
		if (ColumIndex == 0)
		{

			if (!_NPS_View->Visible)
			{
				if (_NPS_View->IsDisposed)
				{
					_NPS_View = gcnew ImageView();
				}
				_NPS_View->SetBufferData(_DataForKernel->GetNPS_Data()[RowIndex].Frame, _DataForKernel->GetNPS_Data()[RowIndex].FrameWidth, _DataForKernel->GetNPS_Data()[RowIndex].FrameHeight);

				Caption = String::Format("NPS_{0}-{1}{2}", gcnew String((char*)_DataForKernel->GetNPS_Data()[RowIndex].Name), NPS_dataGridView->Rows[RowIndex]->Cells[1]->Value, UnitEnergy->HeaderText);

				_NPS_View->SetTitle(Caption);
				_NPS_View->BinariesFrame();
				_NPS_View->Show();

			}

			else
			{
				Caption = String::Format("NPS_{0}-{1}{2}", gcnew String((char*)_DataForKernel->GetNPS_Data()[RowIndex].Name), NPS_dataGridView->Rows[RowIndex]->Cells[1]->Value, UnitEnergy->HeaderText);
				_NPS_View->SetBufferData(_DataForKernel->GetNPS_Data()[RowIndex].Frame, _DataForKernel->GetNPS_Data()[RowIndex].FrameWidth, _DataForKernel->GetNPS_Data()[RowIndex].FrameHeight);
				_NPS_View->SetTitle(Caption);
				_NPS_View->BinariesFrame();

			}
		}
	}

}

private: System::Void MTF_dataGridView_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {

	

	int RowIndex = MTF_dataGridView->CurrentCell->RowIndex;
	int ColumIndex = MTF_dataGridView->CurrentCell->ColumnIndex;
	String^ Caption;
	if (ColumIndex == 1)
	{

		if (!_DQE_View->Visible)
		{
			if (_DQE_View->IsDisposed)
			{
				_DQE_View = gcnew ImageView();
			}
			
			if (_DataForKernel->GetMTF_Data()[RowIndex].Frame)
			{

				Caption = String::Format("MTF_{0} ��� {1}", gcnew String((char*)_DataForKernel->GetMTF_Data()[RowIndex].Name), MTF_dataGridView->Rows[RowIndex]->Cells[0]->Value);
				_DQE_View->SetBufferData(_DataForKernel->GetMTF_Data()[RowIndex].Frame, _DataForKernel->GetMTF_Data()[RowIndex].FrameWidth, _DataForKernel->GetMTF_Data()[RowIndex].FrameHeight);
				_DQE_View->SetTitle(Caption);

				_DQE_View->SetTitle(Caption);
				_DQE_View->BinariesFrame();
				_DQE_View->Show();
			}

		}

		else
		{

			if (_DataForKernel->GetMTF_Data()[RowIndex].Frame)
			{

				Caption = String::Format("MTF_{0} ��� {1}", gcnew String((char*)_DataForKernel->GetMTF_Data()[RowIndex].Name), MTF_dataGridView->Rows[RowIndex]->Cells[0]->Value);
				_DQE_View->SetBufferData(_DataForKernel->GetMTF_Data()[RowIndex].Frame, _DataForKernel->GetMTF_Data()[RowIndex].FrameWidth, _DataForKernel->GetMTF_Data()[RowIndex].FrameHeight);
				_DQE_View->SetTitle(Caption);
				_DQE_View->BinariesFrame();
			}

		}
	}


}
};
}

