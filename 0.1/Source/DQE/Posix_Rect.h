#pragma once
#include "Point.h"
#ifndef  _RECT_
#define  _RECT_
class Posix_Rect
{

public:
	Posix_Rect();
    ~Posix_Rect(void);
	Posix_Rect(size_t l, size_t t, size_t r, size_t b);
	void SetRect(size_t l, size_t t, size_t r, size_t b);
	void SetRectFromDiagonal(size_t X0, size_t Y0, size_t X1, size_t Y1);

	size_t GetHeight(void) { return  bottom - top; };
	size_t GetWidth(void) { return right - left; };
	size_t GetArea(void) { return GetHeight() * GetWidth();};
	size_t GetLeft(void) { return left; };
	size_t GetRight(void) { return  right; };
	size_t GetTop(void) { return top; };
	size_t GetBotton(void) { return bottom; };

	size_t top;
	size_t bottom;
	size_t right;
	size_t left;

private:
	
 
};
#endif
