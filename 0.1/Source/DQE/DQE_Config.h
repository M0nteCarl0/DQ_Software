#pragma once
#include "ConfigParcer.h"
#include <stdint.h>
#include <string>

enum Rotate_Angle
{
	Rotate_Angle_none = 0,
	Rotate_Angle_CW = 1,
	Rotate_Angle_CWW = 2,

};


enum  NPS_DosaUnitType
{
	NPS_DosaUnitType_mkGr = 2,
	NPS_DosaUnitType_mAs = 0,
	NPS_DosaUnitType_mA = 1,
};


enum  Device_Type
{
	Device_Type_UserDefined = 2,
	Device_Type_Prograph = 0,
	Device_Type_Mamo = 1,
	Device_Type_Proscann,

};

enum  MTF_Width
{
	MTF_Width_64pix,
	MTF_Width_128pix,
	MTF_Width_256pix,
	MTF_Width_512pix

};



class DQE_Config: public ConfigParcer
{
public:
	bool OpenConfig(void);
	void ReadParams(void);
	void InitDefaultParams(void);
	void SaveChanges(void);
	DQE_Config(void);
	~DQE_Config(void);

	char* GetPathFrames(void) { return _PathFrames; };
	char* GetDeviceType(void) { return _DeviceType; };
	uint16_t  GetNPS_UnitDosa(void) {
		uint16_t Code = 0;
		if (strcmp(_NPS_UnitDosa,"mAs") == 0)
		{
			Code = NPS_DosaUnitType_mAs;
		}


		if (strcmp(_NPS_UnitDosa, "mA") == 0)
		{
			Code = NPS_DosaUnitType_mA;
		}


		if (strcmp(_NPS_UnitDosa, "mkGr") == 0)
		{
			Code = NPS_DosaUnitType_mkGr;
		}
	
			return Code;
		 };
	uint16_t GetPixelSizeX(void) { return _PixelSizeX; };
	uint16_t GetPixelSizeY(void) { return _PixelSizeY; };
	uint32_t GetSNR(void) { return _SNR; };
	int16_t GetRotate(void) { return _Rotate; };
	uint16_t GetFlipHorizon(void) { return _FlipHorizon; };
	uint16_t GetFlipVertical(void) { return _FlipVertical; };
	uint16_t GetMTF_HeightArea(void) { return _MTF_HeightArea; };
	uint16_t GetMTF_WidthArea(void) { return _MTF_WidthArea; };
	uint16_t GetMTF_ScaleArea(void) 
	{

		if (_MTF_ScaleArea > 16)
		{
			_MTF_ScaleArea = 16;
		}

		if (_MTF_ScaleArea < 0)
		{
			_MTF_ScaleArea = 16;
		}
		return _MTF_ScaleArea;

		
	};
	uint16_t GetNPS_AreaBeginX(void) { return _NPS_AreaBeginX; };
	uint16_t GetNPS_AreaBeginY(void) { return _NPS_AreaBeginY; };
	uint16_t GetNPS_AreaEndX(void) { return _NPS_AreaEndX; };
	uint16_t GetNPS_AreaEndY(void) { return _NPS_AreaEndY; };
	uint64_t GetNPS_AmplitudeDC(void) { return _NPS_AmplitudeDC; };
	float    GetNPS_CoenficinentDosa_mAs(void) { return _NPS_CoenficinentDosa_mAs; };
	float    GetNPS_CoenficinentDosa_mA(void) { return _NPS_CoenficinentDosa_mA; };
	void SetPathFrames(char* PathFrames) { strcpy((char*)_PathFrames, PathFrames); };
	void SetNPS_UnitDoaa(uint16_t *NPS_UnitDosa) {
		switch (*NPS_UnitDosa)
		{
		case NPS_DosaUnitType_mA:
		{
			strcpy(_NPS_UnitDosa, "mA");
			break;
		}

		case NPS_DosaUnitType_mAs:
		{
			strcpy(_NPS_UnitDosa, "mAs");
			break;
		}

		case NPS_DosaUnitType_mkGr:
		{
			strcpy(_NPS_UnitDosa, "mkGr");
			break;
		}

		default:
		{
			strcpy(_NPS_UnitDosa, "mAs");
			break;
		}
		
		}
	};
	void SetPixelSizeX(uint16_t* PixelSizeX) { _PixelSizeX =*PixelSizeX; };
	void SetPixelSizeY(uint16_t* PixelSizeY) { _PixelSizeY = *PixelSizeY;};
	void SetSNR(uint32_t* SNR) { _SNR = *SNR; };
	void SetRotate( int16_t* Rotate) { _Rotate = *Rotate; };
	void SetFlipHorizon(uint16_t* FlipHorizon) { _FlipHorizon = *FlipHorizon; };
	void SetFlipVertical(uint16_t* FlipVertical) { _FlipVertical = *FlipVertical; };
	void SetMTF_HeightArea( uint16_t* MTF_HeightArea) { _MTF_HeightArea = *MTF_HeightArea; };
	void SetMTF_WidthArea(uint16_t* MTF_WidthArea) {  _MTF_WidthArea = *MTF_WidthArea; };
	void SetMTF_ScaleArea(uint16_t* MTF_ScaleArea)
	{
		if (*MTF_ScaleArea > 0 && *MTF_ScaleArea<=16 )
		{
			_MTF_ScaleArea = *MTF_ScaleArea;
		}
		if (*MTF_ScaleArea > 16)
		{
			_MTF_ScaleArea = 16;
		}

		if (*MTF_ScaleArea <=0)
		{
			_MTF_ScaleArea = 1;
		}

	};
	void SetNPS_AreaBeginX(uint16_t* NPS_AreaBeginX){_NPS_AreaBeginX = *NPS_AreaBeginX; };
	void SetNPS_AreaBeginY(uint16_t* NPS_AreaBeginY) { _NPS_AreaBeginY = *NPS_AreaBeginY; };
	void SetNPS_AreaEndX(uint16_t* NPS_AreaEndX) { _NPS_AreaEndX = *NPS_AreaEndX; };
	void SetNPS_AreaEndY(uint16_t* NPS_AreaEndY) { _NPS_AreaEndY= *NPS_AreaEndY; };
	void SetNPS_AmplitudeDC(uint64_t* NPS_AmplitudeDC) { _NPS_AmplitudeDC = *NPS_AmplitudeDC; };
	void SetNPS_CoenficinentDosa_mAs(float*  NPS_CoenficinentDosa_mAs) { _NPS_CoenficinentDosa_mAs = *NPS_CoenficinentDosa_mAs; };
	void SetNPS_CoenficinentDosa_mA(float*  NPS_CoenficinentDosa_mA) { _NPS_CoenficinentDosa_mA = *NPS_CoenficinentDosa_mA; };
	void SetRotateAngle(Rotate_Angle Angle)
	{
		switch (Angle)
		{
		case Rotate_Angle_none:
			_Rotate = 0;
			break;
		case Rotate_Angle_CW:
			_Rotate = -90;
			break;
		case Rotate_Angle_CWW:
			_Rotate = 90;
			break;
		default:
			_Rotate = 0;
			break;
		}
	}
	Rotate_Angle GetRotateAngle(void)
	{
		Rotate_Angle Angle;
		if (_Rotate == 0)
		{
			Angle = Rotate_Angle::Rotate_Angle_none;

		}

		if (_Rotate == -90)
		{
			Angle = Rotate_Angle::Rotate_Angle_CW;

		}
		

		if (_Rotate == 90)
		{
			Angle = Rotate_Angle::Rotate_Angle_CWW;
		}
		return Angle;

	}
	MTF_Width GetMTFWidth(void)
	{
		MTF_Width Width;
		if (_MTF_WidthArea!=256 || _MTF_WidthArea!=128 || _MTF_WidthArea!=64 || _MTF_WidthArea!=512)
		{
			Width = MTF_Width::MTF_Width_64pix;
		}


		if (_MTF_WidthArea == 512)
		{
			Width = MTF_Width::MTF_Width_512pix;
		}


		if (_MTF_WidthArea == 256)
		{
			Width = MTF_Width::MTF_Width_256pix;
		}


		if (_MTF_WidthArea == 128)
		{
			Width = MTF_Width::MTF_Width_128pix;
		}


		if (_MTF_WidthArea == 64)
		{
			Width = MTF_Width::MTF_Width_64pix;
		}

	

		return Width;
	}
	void SetDevicceTypeToMamo(void)
	{
		strcpy(_DeviceType, "Mamo");
		uint16_t Code = NPS_DosaUnitType_mA;
		SetNPS_UnitDoaa(&Code);
	};
	void SetDevicceTypeIoProscann(void)
	{
		strcpy(_DeviceType, "Proscann");
		uint16_t Code = NPS_DosaUnitType_mA;
		SetNPS_UnitDoaa(&Code);
	};
	void SetDevicceTypeToPrograph(void)
	{
		strcpy(_DeviceType, "Prograph");
		uint16_t Code = NPS_DosaUnitType_mAs;
		SetNPS_UnitDoaa(&Code);
	};
	void SetDevicceTypeUser(void)
	{
		strcpy(_DeviceType, "USR");
		
	};
	bool DevicceTypeIsMamo(void)
	{
		bool flag = false;
		if (strcmp(_DeviceType, "Mamo") == 0)
		{
			flag = true;
		}
		return  flag;
	};
	bool DevicceTypeIsProscann(void)
	{
		bool flag = false;;
		if (strcmp(_DeviceType, "Proscann") == 0)
		{
			flag = true;
		}
		return  flag;
	};
	bool DevicceTypeIsPrograph(void)
	{
		bool flag = false;
		if (strcmp(_DeviceType, "Prograph") == 0)
		{
			flag = true;
		}
		return  flag;
	};
	bool DevicceTypeUser(void)
	{
		bool flag= false;
		if (strcmp(_DeviceType, "USR") == 0)
		{
			flag = true;
		}
		return  flag;
	};
private:

	void SetDeviceType(char* DeviceType) { strcpy(_DeviceType, DeviceType); };
    char* _PathFrames;
    char* _DeviceType;
	char* _NPS_UnitDosa;
	uint16_t _PixelSizeX;
	uint16_t _PixelSizeY;
	uint32_t _SNR;
	int16_t _Rotate;
	uint16_t _FlipHorizon;
	uint16_t _FlipVertical;
	uint16_t _MTF_HeightArea;
	uint16_t _MTF_WidthArea;
	uint16_t _MTF_ScaleArea;
	uint16_t _NPS_AreaBeginX;
	uint16_t _NPS_AreaBeginY;
	uint16_t _NPS_AreaEndX;
	uint16_t _NPS_AreaEndY;
	uint64_t _NPS_AmplitudeDC;
	float    _NPS_CoenficinentDosa_mAs;
	float    _NPS_CoenficinentDosa_mA;
	char* _Buffer;
};

