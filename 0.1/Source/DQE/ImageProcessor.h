#pragma once
#include <stdint.h>
#include "Posix_Rect.h"
#include <math.h>
#ifndef  _IMAGEPROCESSOR_
#define  _IMAGEPROCESSOR_
class ImageProcessor
{
public:
	ImageProcessor(void);
	~ImageProcessor(void);
template<typename T>	double ComputeRMSImage(T* ImageBuffer,size_t W,size_t H,Posix_Rect* ROI  =  nullptr);
template<typename T>	double ComputeAvergageImage(T* ImageBuffer,size_t W,size_t H,Posix_Rect* ROI  = nullptr);
template<typename T>	void  FlipImage(T* ImageBuffer,size_t W,size_t H,bool ByHorizontalAxis,bool ByVerticalAxis);
template<typename T>	void  RotateImage(T* ImageBuffer,size_t W,size_t H,size_t AngleInDegree);
};
#endif
/************************************************/
template<typename T>
inline double ImageProcessor::ComputeRMSImage(T * ImageBuffer, size_t W, size_t H, Posix_Rect * ROI)
{

	double RMS = 0.0;
	double Avergage = 0.0;

	if (ROI != nullptr)
	{
		for (size_t i = ROI->GetTop(); i < ROI->GetBotton(); i++)//������ 
		{
			for (size_t j = ROI->GetLeft(); j < ROI->GetRight(); j++)//�������
			{
				Avergage += (double)ImageBuffer[i*W + j];
			}
		}

		Avergage /= (float)ROI->GetArea();

		for (size_t i = ROI->GetTop(); i < ROI->GetBotton(); i++)//������ 
		{
			for (size_t j = ROI->GetLeft(); j < ROI->GetRight(); j++)//�������
			{
				RMS += pow((double)(ImageBuffer[i*W + j] - Avergage),2.0);
			}
		}

		RMS = sqrtf(RMS / (double)ROI->GetArea());

	}
	
	return RMS;
}
/************************************************/
template<typename T>
inline double ImageProcessor::ComputeAvergageImage(T * ImageBuffer, size_t W, size_t H, Posix_Rect * ROI)
{
	double Avergage = 0.0;

	if (ROI!=nullptr)
	{
		for (size_t i = ROI->GetTop() ; i < ROI->GetBotton(); i++)//������ 
		{
			for (size_t j = ROI->GetLeft(); j < ROI->GetRight(); j++)//�������
			{
				Avergage += (double)ImageBuffer[i*W + j];
			}
		}

		Avergage /= (double)ROI->GetArea();
	}
	else
	{
		for (size_t i = 0; i < W*H; i++)
		{
			Avergage += (double)ImageBuffer[i];
		}
		Avergage /= (double)(W*H);
	}


	return Avergage;
}
/************************************************/
template<typename T> inline void  ImageProcessor::FlipImage(T* ImageBuffer, size_t W, size_t H, bool ByHorizontalAxis, bool ByVerticalAxis)
{
	size_t TotatalPixels = W * H;

	if (ByHorizontalAxis)
	{

		T* TempmBuffer = new T[TotatalPixels];
		memcpy(TempmBuffer, ImageBuffer, sizeof(T)* TotatalPixels);
		size_t Horizontalflip = H - 1;
		//#pragma omp_parallel  for  shared(ImageBuffer) private(TotatalPixels)
		for (size_t i = 0; i < H; i++)//������
		{
			for (size_t j = 0; j < W; j++)//�������
			{
				ImageBuffer[i*W + j] = TempmBuffer[Horizontalflip*W + j];//Position Element:i*H+j;
			}
			Horizontalflip--;
		}
		//#pragma omp barrier

		delete[] TempmBuffer;
	}
	

	if (ByVerticalAxis)
	{
		T* TempmBuffer = new T[TotatalPixels];
		memcpy(TempmBuffer, ImageBuffer, sizeof(T)* TotatalPixels);
		for (size_t i = 0; i < H; i++)//������ 
		{
			size_t Vericalflip = W - 1;
			for (size_t j = 0; j < W; j++)//�������
			{
				ImageBuffer[i*W + j] = TempmBuffer[i*W + Vericalflip];//Position Element:i*H+j;
				Vericalflip--;
			}
		}
		//#pragma omp barrier
		delete[] TempmBuffer;
	}

}
/************************************************/
template<typename T>
inline void ImageProcessor::RotateImage(T * ImageBuffer, size_t W, size_t H, size_t AngleInDegree)
{
	float DegreesToRadians = AngleInDegree * (3.14159265358979323846/180);
	size_t TotatalPixels = W * H;
	T* TempmBuffer = new T[TotatalPixels];
	memcpy(TempmBuffer, ImageBuffer, sizeof(T)* TotatalPixels);

	for (size_t i = 0; i < H; i++)//������ 
	{

		for (size_t j = 0; j < W; j++)//�������
		{
			int RotateX = abs( i*cos(DegreesToRadians) - j*sin(DegreesToRadians));
			int RotateY = abs( i*sin(DegreesToRadians) + j*cos(DegreesToRadians));
			ImageBuffer[i*W + j] = TempmBuffer[RotateX*W + RotateY];

		}

	}
	delete[] TempmBuffer;
}
