#include <stdint.h>
#include <vector>
#pragma once
#ifndef _PROXY_
#define _PROXY_ 
using namespace std;
typedef struct MTF_DataContainer
{
	uint16_t* Frame;
	uint8_t* Name;
	size_t FrameWidth;
	size_t FrameHeight;
	size_t WidthArea;
	size_t HeigthArea;
	size_t ScaleArea;
	uint8_t Result;
}MTF_DataContainer;

typedef struct NPS_DataContainer
{
	uint16_t* Frame;
	uint8_t* Name;
	size_t FrameWidth;
	size_t FrameHeight;
	size_t BeginX;
	size_t BeginY;
	size_t EndX;
	size_t EndY;
	float DosaCoff;
	uint8_t Result;
}NPS_DataContainer;
#endif
class Transaction
{
public:
	Transaction();
	~Transaction();
	std::vector<MTF_DataContainer> GetMTF_Data(void);
	std::vector<NPS_DataContainer> GetNPS_Data(void);
	void PutNPSData(uint8_t* Name, uint16_t* Frame, size_t FrameWidth,
		size_t FrameHeight,
		size_t BeginX,
		size_t BeginY,
		size_t EndX,
		size_t EndY);
	void RemoveNPSData(size_t ID);
	void PutMTFData(size_t Axis,uint8_t* Name, uint16_t* Frame, size_t FrameWidth,
		size_t FrameHeight,
		size_t WidthArea,
		size_t HeigthArea,
		size_t ScaleArea);
	void RemoveMTFData(size_t ID);

	void Clean_MTFData(void);
	void Clean_NPSData(void);

private:
	std::vector<MTF_DataContainer> _MTF_Data;
	std::vector<NPS_DataContainer> _NPS_Data;
};


