#include "ProxyTransaction.h"
void Transaction::Clean_MTFData()
{
	if (_MTF_Data.size() != 0)
	{

		for (size_t i = 0; i < _MTF_Data.size(); i++)
		{

			if (_MTF_Data[i].Frame)
			{
				delete[] _MTF_Data[i].Frame;
				_MTF_Data[i].Frame = nullptr;

			}
		}


	}

}


void Transaction:: Clean_NPSData()
{

	if (_NPS_Data.size() != 0)
	{

		for (size_t i = 0; i < _NPS_Data.size(); i++)
		{
			if (_NPS_Data[i].Frame)
			{
				delete[] _NPS_Data[i].Frame;
				_NPS_Data[i].Frame = nullptr;

			}

		}
	}

}


std::vector<MTF_DataContainer>  Transaction::GetMTF_Data(void)
{
	return _MTF_Data;

}
std::vector<NPS_DataContainer> Transaction::GetNPS_Data(void)
{
	return _NPS_Data;
}
void  Transaction::RemoveNPSData(size_t ID)
{
	if (_NPS_Data[ID].Frame)
	{
		delete[] _NPS_Data[ID].Frame;
		delete[] _NPS_Data[ID].Name;
		_NPS_Data[ID].Frame = nullptr;
		_NPS_Data[ID].Name = nullptr;

	}
	_NPS_Data.erase(_NPS_Data.begin() + ID);
	_NPS_Data.shrink_to_fit();
}


void  Transaction::PutNPSData(uint8_t* Name, uint16_t* Frame, size_t FrameWidth,
	size_t FrameHeight,
	size_t BeginX,
	size_t BeginY,
	size_t EndX,
	size_t EndY)
{
	NPS_DataContainer Data;
	Data.Frame = new uint16_t[FrameHeight*FrameHeight];
	Data.Name = new uint8_t[strlen((char*)Name)];
	memcpy(Data.Frame, Frame, FrameHeight*FrameWidth * sizeof(uint16_t));
	Data.FrameHeight = FrameHeight;
	Data.FrameWidth = FrameWidth;
	Data.BeginX = BeginX;
	Data.BeginY = BeginY;
	Data.EndX = EndX;
	Data.EndY = EndY;
	strcpy((char*)Data.Name, (char*)Name);
	_NPS_Data.push_back(Data);


}


void Transaction::PutMTFData(size_t Axis,uint8_t* Name, uint16_t* Frame, size_t FrameWidth,
	size_t FrameHeight,
	size_t WidthArea,
	size_t HeigthArea,
	size_t ScaleArea)
{

    MTF_DataContainer Data;
	if (_MTF_Data[Axis].Frame)
	{
		delete[] _MTF_Data[Axis].Frame;
		_MTF_Data[Axis].Frame = nullptr;
		delete[] _MTF_Data[Axis].Name;
		_MTF_Data[Axis].Name = nullptr;
	}


	Data.Frame = new uint16_t[FrameHeight*FrameHeight];
	Data.Name = new uint8_t[strlen((char*)Name)];

	memcpy(Data.Frame, Frame, FrameHeight*FrameWidth * sizeof(uint16_t));
	Data.FrameHeight = FrameHeight;
	Data.FrameWidth = FrameWidth;
	Data.HeigthArea = HeigthArea;
	Data.WidthArea = WidthArea;
	Data.ScaleArea = ScaleArea;
	strcpy((char*)Data.Name, (char*)Name);
	_MTF_Data[Axis] = Data;

}

void Transaction::RemoveMTFData(size_t ID)
{


	if (_MTF_Data[ID].Frame)
	{
		delete[] _MTF_Data[ID].Frame;
		delete[] _MTF_Data[ID].Name;
		_MTF_Data[ID].Frame = nullptr;
		_MTF_Data[ID].Name = nullptr;

	}


}



Transaction::Transaction()
{
	_MTF_Data = std::vector<MTF_DataContainer>(2);
}

Transaction::~Transaction()
{
	Clean_MTFData();
	Clean_NPSData();
}