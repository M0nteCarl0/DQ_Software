#include "DQE_Config.h"
#include <string>
#include <sstream>
const char* Filename = "DQE.conf";

bool DQE_Config::OpenConfig(void)
{
	bool Result = false;
	BEG:
	if (Open(Filename))
	{
		 ReadParams();
		 Close();
		Result = true;
	}
	else
	{
		InitDefaultParams();
		SaveChanges();
		goto BEG;
		
	} 
	
	return Result;
}

void DQE_Config::ReadParams(void)
{
	

		GetTemplateParam("PathFrames", _PathFrames);
		GetTemplateParam("DeviceType", _DeviceType);
		GetTemplateParam("NPS_UnitDosa", _NPS_UnitDosa);
		GetTemplateParam("PixelSizeX", _PixelSizeX);
		GetTemplateParam("PixelSizeY", _PixelSizeY);
		GetTemplateParam("SNR", _SNR);
		GetTemplateParam("Rotate", _Rotate);
		GetTemplateParam("FlipHorizon", _FlipHorizon);
		GetTemplateParam("FlipVertical", _FlipVertical);

		GetTemplateParam("MTF_HeightArea", _MTF_HeightArea);
		GetTemplateParam("MTF_WidthArea", _MTF_WidthArea);
		GetTemplateParam("MTF_ScaleArea", _MTF_ScaleArea);

		GetTemplateParam("NPS_AreaBeginX", _NPS_AreaBeginX);
		GetTemplateParam("NPS_AreaBeginY", _NPS_AreaBeginY);
		GetTemplateParam("NPS_AreaEndX", _NPS_AreaEndX);
		GetTemplateParam("NPS_AreaEndY", _NPS_AreaEndY);
		GetTemplateParam("NPS_AmplitudeDC", _NPS_AmplitudeDC);
		GetTemplateParam("NPS_CoenficinentDosa_mAs", _NPS_CoenficinentDosa_mAs);
		GetTemplateParam("NPS_CoenficinentDosa_mA", _NPS_CoenficinentDosa_mA);
		

	
}

void DQE_Config::InitDefaultParams(void)
{

	
	strcpy(_PathFrames, "Proba");
	uint16_t Unit = NPS_DosaUnitType_mAs;
	SetDevicceTypeUser();
	SetNPS_UnitDoaa(&Unit);
	 _PixelSizeX = 144;
	 _PixelSizeY = 144;
	 _SNR =30174;
	 _Rotate=0;
	 _FlipHorizon=0;
	 _FlipVertical=0;
	 _MTF_HeightArea=200;
	 _MTF_WidthArea=256;
	 _MTF_ScaleArea=4;
	 _NPS_AreaBeginX=0;
	 _NPS_AreaBeginY=0;
	 _NPS_AreaEndX=1024;
	 _NPS_AreaEndY=1024;
	 _NPS_AmplitudeDC=1000;
	 _NPS_CoenficinentDosa_mAs=2.0;
	 _NPS_CoenficinentDosa_mA=4.6;


}

void DQE_Config::SaveChanges(void)
{

	if (_File.is_open())
	{
		_File.close();
	}

	_File = fstream(Filename,std::ios::out);
	
	if (_File.is_open())
	{

		ios::sync_with_stdio(false);
		stringstream _SS;
		_File.rdbuf()->pubsetbuf(_Buffer, 4096);
		_File.seekg(0, ios_base::beg);
		_File << "PathFrames=" << _PathFrames << "\n"
			<< "DeviceType=" << _DeviceType << "\n"
			<< "PixelSizeX=" << _PixelSizeX << "\n"
			<< "PixelSizeY=" << _PixelSizeY << "\n"
			<< "SNR=" << _SNR << "\n"
			<< "Rotate=" << _Rotate << "\n"
			<< "FlipHorizon=" << _FlipHorizon << "\n"
			<< "FlipVertical=" << _FlipVertical << "\n"
			<< "MTF_HeightArea=" << _MTF_HeightArea << "\n"
			<< "MTF_WidthArea=" << _MTF_WidthArea << "\n"
			<< "MTF_ScaleArea=" << _MTF_ScaleArea << "\n"
			<< "NPS_AreaBeginX=" << _NPS_AreaBeginX << "\n"
			<< "NPS_AreaBeginY=" << _NPS_AreaBeginY << "\n"
			<< "NPS_AreaEndX=" << _NPS_AreaEndX << "\n"
			<< "NPS_AreaEndY=" << _NPS_AreaEndY << "\n"
			<< "NPS_AmplitudeDC=" << _NPS_AmplitudeDC << "\n"
			<< "NPS_UnitDosa=" << _NPS_UnitDosa << "\n"
			<< "NPS_CoenficinentDosa_mAs=" << _NPS_CoenficinentDosa_mAs << "\n"
			<< "NPS_CoenficinentDosa_mA=" << _NPS_CoenficinentDosa_mA << "\n";
			_File.sync();
			_File.flush();
			_File.close();





	}
}



DQE_Config::DQE_Config(void)
{
 SetDelimetrToken("=");
 _NPS_UnitDosa = nullptr;
 _PathFrames = nullptr;
 _DeviceType = nullptr;
 _PathFrames = new char[65000];
 _DeviceType = new char[_MAX_PATH];
 _NPS_UnitDosa = new char[_MAX_PATH];
 memset(_PathFrames, 0, _MAX_PATH);
 memset(_DeviceType, 0, _MAX_PATH);
 memset(_NPS_UnitDosa, 0, _MAX_PATH);
 _PixelSizeX=0;
 _PixelSizeY=0;
 _SNR=0;
 _Rotate=0;
 _FlipHorizon=0;
 _FlipVertical=0;
 _MTF_HeightArea=0;
 _MTF_WidthArea=0;
 _MTF_ScaleArea=0;
 _NPS_AreaBeginX= 0;
 _NPS_AreaBeginY = 0;
 _NPS_AreaEndX = 0;
 _NPS_AreaEndY = 0;
 _NPS_AmplitudeDC = 0;
 _NPS_CoenficinentDosa_mAs = 0.0;
 _NPS_CoenficinentDosa_mA = 0.0;
 _Buffer = new char[4096];

}


DQE_Config::~DQE_Config(void)
{
	if (_Buffer)
	{
		delete[] _Buffer;
	}

	if (_PathFrames)
	{
		delete []  _PathFrames;
	}

	if (_DeviceType)
	{
		delete[] _DeviceType;
	}

	if (_NPS_UnitDosa)
	{
		delete[] _NPS_UnitDosa;

	}

}
