
#include "LUT_Solver.h"
#include <math.h>
/********************************************************************/
LUT_Solver::LUT_Solver(void)
{
	_BlackPixel		= 0;
	_WhitePixel		= 0;
	_AreaOfImage	= 0;
	_AreaOfThersholdBlack = 0;
	_AreaOfThersholdWhite = 0;
	_PercentsAreaOfThersholdBlack = 0;
	_PercentsAreaOfThersholdWhite = 0;
    _BitsPerPixel = 8;
	_MaxValuePalitra = 0;
	_LUT = nullptr;

}
/********************************************************************/
LUT_Solver::~LUT_Solver(void)
{
	
}
/********************************************************************/
void LUT_Solver::ComputeThershold(Histogram * Histogram,size_t ImageH,size_t ImageW, float PercentsOfAreaWhite , float PercentsOfAreaBlack, bool UseFastLUT)
{
	_AreaOfImage = ImageH * ImageW;
	_PercentsAreaOfThersholdBlack = PercentsOfAreaBlack;
	_PercentsAreaOfThersholdWhite = PercentsOfAreaWhite;
	_AreaOfThersholdBlack =(size_t)( _AreaOfImage* (PercentsOfAreaBlack* 0.01));
	_AreaOfThersholdWhite = (size_t)(_AreaOfImage* (PercentsOfAreaWhite* 0.01));
	size_t BinCount = Histogram->GetBinCount();
	size_t ComputedArea = 0;
    _ScaleFactor = 0;
	_WhitePixel = 0;
	_BlackPixel = 0;
	_MaxValuePalitra = (size_t)pow(2.0, (double)_BitsPerPixel);
#pragma omp_parallel  for  shared(_BlackPixel,Histogram) private(BinCount,ComputedArea,_AreaOfThershold)
	{
		for (size_t i = 0; i < BinCount; i++)
		{
			ComputedArea+= Histogram->GetBinByIndex(i);
			if (ComputedArea > _AreaOfThersholdBlack)
			{
				_BlackPixel = i;
				 break;
			}
			

		}
	}
#pragma omp barrier
ComputedArea = 0;
#pragma omp_parallel  for  shared(_WhitePixel,Histogram) private(BinCount,ComputedArea,_AreaOfThershold)
	{
		for (size_t i = BinCount-1; i > 0; i--)
		{
			ComputedArea += Histogram->GetBinByIndex(i);
			if (ComputedArea > _AreaOfThersholdWhite)
			{
				_WhitePixel = i;
				break;
			}
			

		}
	}
#pragma omp barrier
	_ScaleFactor  =(float)((float)_MaxValuePalitra/((float)_WhitePixel - (float)_BlackPixel));
	if (UseFastLUT)
	{
		if (_LUT)
		{
			delete[] _LUT;
			_LUT = nullptr;
		}
		_LUT = new uint16_t[Histogram->_Max];
		memset(_LUT, 0, sizeof(uint16_t) * Histogram->_Max);
//#pragma omp_parallel  for  shared(_LUT) private(BinCount)
		for (uint16_t i = 0; i < Histogram->_Max; i++)
		{
			_LUT[i] = GetPixelFromLUT(&i);
		}
//#pragma omp barrier

	}
	


}
/********************************************************************/
short LUT_Solver::GetPixelFromLUT(uint16_t * RawPixel)
{
	short Pixel = 0;


	if (_WhitePixel == 0 && _BlackPixel == 0 )
	{
		Pixel = 0;
	}
	else
	{

		if (*RawPixel >= _BlackPixel || *RawPixel <= _WhitePixel)
		{
			Pixel = (uint16_t)((float)_ScaleFactor * (*RawPixel - _BlackPixel));
		}
		if (*RawPixel <= _BlackPixel)
		{
			Pixel = 0;
		}

		if (*RawPixel >= _WhitePixel)
		{
			Pixel = _MaxValuePalitra - 1;
		}

	}

	
	return Pixel;
}
/********************************************************************/
short LUT_Solver::GetRealPixelValue(uint8_t Color)
{
	short Result = ((float)Color/_ScaleFactor) + _BlackPixel;
	return  Result;
}
/********************************************************************/
