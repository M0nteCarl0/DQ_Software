#pragma once
#include "DQE_Config.h"
#include <Windows.h>
namespace DQE {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;
	

	/// <summary>
	/// ������ ��� DQE_Parametrs
	/// </summary>
	public ref class Compute_Parametrs : public System::Windows::Forms::Form
	{
	public:
		String^ Path;
		HWND MainViewHandle;
		Compute_Parametrs(void)
		{
			InitializeComponent();
			_DQE_Config = new DQE_Config();


			if (_DQE_Config->OpenConfig())
			{
			
				FramePath->Text = gcnew String(Marshal::PtrToStringAnsi((IntPtr)(char*)_DQE_Config->GetPathFrames()));
				PixelSizeX->Text = _DQE_Config->GetPixelSizeX().ToString();
				PixelSizeY->Text = _DQE_Config->GetPixelSizeY().ToString();
				SNR->Text = _DQE_Config->GetSNR().ToString();
				FlipHorizontal->Checked =(bool) _DQE_Config->GetFlipHorizon();
				FlipVertical->Checked = (bool)_DQE_Config->GetFlipVertical();
				MTF_HeightArea->Text = _DQE_Config->GetMTF_HeightArea().ToString();
				NPS_BeginX->Text = _DQE_Config->GetNPS_AreaBeginX().ToString();
				NPS_BeginY->Text = _DQE_Config->GetNPS_AreaBeginY().ToString();
				NPS_EndX->Text = _DQE_Config->GetNPS_AreaEndX().ToString();
				NPS_EndY->Text = _DQE_Config->GetNPS_AreaEndY().ToString();
				NPS_AmplitudeDC->Text = _DQE_Config->GetNPS_AmplitudeDC().ToString();

				MTF_WidthtArea->SelectedIndex = _DQE_Config->GetMTFWidth();
				MTF_ScaleArea->SelectedIndex = _DQE_Config->GetMTF_ScaleArea()-1;
				if (_DQE_Config->DevicceTypeIsMamo())
				{
					DeviceType->SelectedIndex = 1;
				}


				if (_DQE_Config->DevicceTypeIsPrograph())
				{
					DeviceType->SelectedIndex = 3;
				}

				if (_DQE_Config->DevicceTypeIsProscann())
				{
					DeviceType->SelectedIndex = 2;
				}


				if (_DQE_Config->DevicceTypeUser())
				{
					DeviceType->SelectedIndex = 0;
				}

				if (_DQE_Config->GetFlipVertical())
				{
					FlipVertical->Checked = true;
				}

				if (_DQE_Config->GetFlipHorizon())
				{
					FlipHorizontal->Checked = true;
				}

				int Angle = _DQE_Config->GetRotateAngle();
				switch (Angle)
				{

					case Rotate_Angle::Rotate_Angle_CW:
					{
						Rotate->SelectedIndex = 1;
						break;
					}


					case Rotate_Angle::Rotate_Angle_CWW:
					{

						Rotate->SelectedIndex = 2;
						break;
					}

					case Rotate_Angle::Rotate_Angle_none:
					{

						Rotate->SelectedIndex = 0;
						break;
					}


				}



				uint16_t NPS_Init = _DQE_Config->GetNPS_UnitDosa();
				NPS_UnitDosa->SelectedIndex = NPS_Init;

				if (_DQE_Config->GetNPS_UnitDosa() == NPS_DosaUnitType_mkGr)
				{
					NPS_Coenficient_of_Dosa->Visible = false;
					NPS_DosaCoffTitle->Visible = false;

				}
				else
				{
					NPS_Coenficient_of_Dosa->Visible = true;
					NPS_DosaCoffTitle->Visible = true;
					
					if (_DQE_Config->GetNPS_UnitDosa() == NPS_DosaUnitType_mAs)
					{
						NPS_Coenficient_of_Dosa->Text = _DQE_Config->GetNPS_CoenficinentDosa_mAs().ToString();
					}

					if (_DQE_Config->GetNPS_UnitDosa() == NPS_DosaUnitType_mA)
					{
						NPS_Coenficient_of_Dosa->Text = _DQE_Config->GetNPS_CoenficinentDosa_mA().ToString();
					}


				}
				

				




			}

			//
			//TODO: �������� ��� ������������
			//
		}
		void WriteParametrs(void)
		{



			uint16_t _PixelSizeX;
			uint16_t _PixelSizeY;
			uint32_t _SNR;
			Rotate_Angle _Rotate;
			uint16_t _FlipHorizon = 0;
			uint16_t _FlipVertical = 0;
			uint16_t _MTF_HeightArea;
			uint16_t _MTF_WidthArea;
			uint16_t _MTF_ScaleArea;
			uint16_t _NPS_AreaBeginX;
			uint16_t _NPS_AreaBeginY;
			uint16_t _NPS_AreaEndX;
			uint16_t _NPS_AreaEndY;
			uint64_t _NPS_AmplitudeDC;
			uint16_t _NPS_UnitDosa;
			float    _NPS_CoenficinentDosa_mAs;
			float    _NPS_CoenficinentDosa_mA;

			uint16_t::TryParse(PixelSizeX->Text, _PixelSizeX);
			uint16_t::TryParse(PixelSizeY->Text, _PixelSizeY);
			uint16_t::TryParse(MTF_HeightArea->Text, _MTF_HeightArea);

			uint16_t::TryParse(NPS_BeginX->Text, _NPS_AreaBeginX);
			uint16_t::TryParse(NPS_BeginY->Text, _NPS_AreaBeginY);
			uint16_t::TryParse(NPS_EndX->Text, _NPS_AreaEndX);
			uint16_t::TryParse(NPS_EndY->Text, _NPS_AreaEndY);
			uint32_t::TryParse(SNR->Text, _SNR);

			uint64_t::TryParse(NPS_AmplitudeDC->Text, _NPS_AmplitudeDC);


			if (DeviceType->SelectedIndex == 3)
			{
				_DQE_Config->SetDevicceTypeToPrograph();
			}

			if (DeviceType->SelectedIndex == 2)
			{

				_DQE_Config->SetDevicceTypeIoProscann();
			}


			if (DeviceType->SelectedIndex == 0)
			{
				_DQE_Config->SetDevicceTypeUser();

			}

			if (DeviceType->SelectedIndex == 1)
			{
				_DQE_Config->SetDevicceTypeToMamo();

			}

			_MTF_ScaleArea = MTF_ScaleArea->SelectedIndex + 1;
			_DQE_Config->SetMTF_ScaleArea(&_MTF_ScaleArea);

			switch (Rotate->SelectedIndex)
			{
			
			case 0:
				_Rotate = Rotate_Angle_none;
				break;
			case 1:
				_Rotate = Rotate_Angle_CW;
				break;
			case 2:
				_Rotate = Rotate_Angle_CWW;
				break;
			default:
				
				break;
			}
			
			if (Path!=nullptr)
			{
				_DQE_Config->SetPathFrames((char*)Marshal::StringToHGlobalAnsi(Path).ToPointer());
			}
			
			_DQE_Config->SetRotateAngle(_Rotate);


			if (DeviceType->SelectedIndex == 0 )
			{

				_NPS_UnitDosa = NPS_UnitDosa->SelectedIndex;
				_DQE_Config->SetNPS_UnitDoaa(&_NPS_UnitDosa);
				if (NPS_UnitDosa->SelectedIndex == NPS_DosaUnitType_mAs)
				{
					float::TryParse(NPS_Coenficient_of_Dosa->Text, _NPS_CoenficinentDosa_mAs);
					_DQE_Config->SetNPS_CoenficinentDosa_mAs(&_NPS_CoenficinentDosa_mAs);
				}

				if (NPS_UnitDosa->SelectedIndex == NPS_DosaUnitType_mA)
				{
					float::TryParse(NPS_Coenficient_of_Dosa->Text, _NPS_CoenficinentDosa_mA);
					_DQE_Config->SetNPS_CoenficinentDosa_mA(&_NPS_CoenficinentDosa_mA);
				}

			}
			uint32_t::TryParse(SNR->Text, _SNR);
			

			if (FlipHorizontal->Checked)
			{
				_FlipHorizon = 1;
			}


			if (FlipVertical->Checked)
			{
				_FlipVertical = 1;
			}

		








			_DQE_Config->SetFlipHorizon(&_FlipHorizon);
			_DQE_Config->SetFlipVertical(&_FlipVertical);
			_DQE_Config->SetNPS_AmplitudeDC(&_NPS_AmplitudeDC);
			_DQE_Config->SetMTF_HeightArea(&_MTF_HeightArea);
			_DQE_Config->SetNPS_AreaBeginX(&_NPS_AreaBeginX);
			_DQE_Config->SetNPS_AreaBeginY(&_NPS_AreaBeginY);
			_DQE_Config->SetNPS_AreaEndX(&_NPS_AreaEndX);
			_DQE_Config->SetNPS_AreaEndY(&_NPS_AreaEndY);
			_DQE_Config->SetSNR(&_SNR);







			//if (_DQE_Config->GetIsConfigFileOpen())
			//{
				_DQE_Config->SaveChanges();
		//	}


		}
private: System::Windows::Forms::FolderBrowserDialog^  PathFrames;
public:
	protected:
		DQE_Config* _DQE_Config;
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Compute_Parametrs()
		{

			if (_DQE_Config)
			{
				delete _DQE_Config;
					 
			}

			if (components)
			{
				delete components;
			}
		}
	protected: 
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::GroupBox^  groupBox3;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  FramePath;


	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  PixelSizeX;
	private: System::Windows::Forms::TextBox^  PixelSizeY;


	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Button^  _Cancel_button;

	private: System::Windows::Forms::Button^  _OK_button;
	private: System::Windows::Forms::ComboBox^  MTF_WidthtArea;


	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::ComboBox^  MTF_ScaleArea;

	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  MTF_HeightArea;


	private: System::Windows::Forms::GroupBox^  groupBox5;
	private: System::Windows::Forms::TextBox^  NPS_EndY;

	private: System::Windows::Forms::TextBox^  NPS_BeginY;
	private: System::Windows::Forms::TextBox^  NPS_EndX;


	private: System::Windows::Forms::TextBox^  NPS_BeginX;

	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::TextBox^  NPS_AmplitudeDC;
	private: System::Windows::Forms::Label^  NPS_DosaCoffTitle;



	private: System::Windows::Forms::TextBox^  NPS_Coenficient_of_Dosa;

	private: System::Windows::Forms::TextBox^  SNR;

	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::Label^  label20;
	private: System::Windows::Forms::Label^  label19;
	private: System::Windows::Forms::Label^  label18;



	private: System::Windows::Forms::Label^  label21;
private: System::Windows::Forms::ComboBox^  DeviceType;



private: System::Windows::Forms::ComboBox^  NPS_UnitDosa;

	private: System::Windows::Forms::Label^  label24;

	private: System::Windows::Forms::Label^  label23;
	private: System::Windows::Forms::GroupBox^  groupBox9;
private: System::Windows::Forms::CheckBox^  FlipHorizontal;

private: System::Windows::Forms::CheckBox^  FlipVertical;

private: System::Windows::Forms::ComboBox^  Rotate;

	private: System::Windows::Forms::Label^  label22;
	private: System::Windows::Forms::GroupBox^  groupBox4;
private: System::Windows::Forms::Label^  label25;









	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->MTF_WidthtArea = (gcnew System::Windows::Forms::ComboBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->MTF_ScaleArea = (gcnew System::Windows::Forms::ComboBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->MTF_HeightArea = (gcnew System::Windows::Forms::TextBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->NPS_DosaCoffTitle = (gcnew System::Windows::Forms::Label());
			this->NPS_Coenficient_of_Dosa = (gcnew System::Windows::Forms::TextBox());
			this->NPS_UnitDosa = (gcnew System::Windows::Forms::ComboBox());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->NPS_AmplitudeDC = (gcnew System::Windows::Forms::TextBox());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->NPS_EndY = (gcnew System::Windows::Forms::TextBox());
			this->NPS_BeginY = (gcnew System::Windows::Forms::TextBox());
			this->NPS_EndX = (gcnew System::Windows::Forms::TextBox());
			this->NPS_BeginX = (gcnew System::Windows::Forms::TextBox());
			this->SNR = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->FramePath = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->PixelSizeX = (gcnew System::Windows::Forms::TextBox());
			this->PixelSizeY = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->groupBox9 = (gcnew System::Windows::Forms::GroupBox());
			this->FlipHorizontal = (gcnew System::Windows::Forms::CheckBox());
			this->FlipVertical = (gcnew System::Windows::Forms::CheckBox());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->Rotate = (gcnew System::Windows::Forms::ComboBox());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->DeviceType = (gcnew System::Windows::Forms::ComboBox());
			this->_Cancel_button = (gcnew System::Windows::Forms::Button());
			this->_OK_button = (gcnew System::Windows::Forms::Button());
			this->PathFrames = (gcnew System::Windows::Forms::FolderBrowserDialog());
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox9->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->label17);
			this->groupBox2->Controls->Add(this->label16);
			this->groupBox2->Controls->Add(this->label15);
			this->groupBox2->Controls->Add(this->MTF_WidthtArea);
			this->groupBox2->Controls->Add(this->label7);
			this->groupBox2->Controls->Add(this->MTF_ScaleArea);
			this->groupBox2->Controls->Add(this->label6);
			this->groupBox2->Controls->Add(this->label5);
			this->groupBox2->Controls->Add(this->MTF_HeightArea);
			this->groupBox2->Location = System::Drawing::Point(7, 264);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(236, 233);
			this->groupBox2->TabIndex = 1;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"MTF";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(185, 79);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(25, 13);
			this->label17->TabIndex = 13;
			this->label17->Text = L"���";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(185, 53);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(25, 13);
			this->label16->TabIndex = 12;
			this->label16->Text = L"���";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(185, 22);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(25, 13);
			this->label15->TabIndex = 11;
			this->label15->Text = L"���";
			// 
			// MTF_WidthtArea
			// 
			this->MTF_WidthtArea->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->MTF_WidthtArea->FormattingEnabled = true;
			this->MTF_WidthtArea->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"64", L"128", L"256", L"512" });
			this->MTF_WidthtArea->Location = System::Drawing::Point(120, 45);
			this->MTF_WidthtArea->Name = L"MTF_WidthtArea";
			this->MTF_WidthtArea->Size = System::Drawing::Size(62, 21);
			this->MTF_WidthtArea->TabIndex = 10;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(14, 74);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(97, 13);
			this->label7->TabIndex = 9;
			this->label7->Text = L"������� �������";
			// 
			// MTF_ScaleArea
			// 
			this->MTF_ScaleArea->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->MTF_ScaleArea->FormattingEnabled = true;
			this->MTF_ScaleArea->Items->AddRange(gcnew cli::array< System::Object^  >(16) {
				L"1", L"2", L"3", L"4", L"5", L"6", L"7", L"8",
					L"9", L"10", L"11", L"12", L"13", L"14", L"15", L"16"
			});
			this->MTF_ScaleArea->Location = System::Drawing::Point(120, 71);
			this->MTF_ScaleArea->Name = L"MTF_ScaleArea";
			this->MTF_ScaleArea->Size = System::Drawing::Size(62, 21);
			this->MTF_ScaleArea->TabIndex = 0;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(15, 48);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(90, 13);
			this->label6->TabIndex = 8;
			this->label6->Text = L"������ �������";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(16, 22);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(89, 13);
			this->label5->TabIndex = 7;
			this->label5->Text = L"������ �������";
			// 
			// MTF_HeightArea
			// 
			this->MTF_HeightArea->Location = System::Drawing::Point(120, 19);
			this->MTF_HeightArea->Name = L"MTF_HeightArea";
			this->MTF_HeightArea->Size = System::Drawing::Size(62, 20);
			this->MTF_HeightArea->TabIndex = 4;
			this->MTF_HeightArea->Text = L"200";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->NPS_DosaCoffTitle);
			this->groupBox3->Controls->Add(this->NPS_Coenficient_of_Dosa);
			this->groupBox3->Controls->Add(this->NPS_UnitDosa);
			this->groupBox3->Controls->Add(this->label24);
			this->groupBox3->Controls->Add(this->label20);
			this->groupBox3->Controls->Add(this->label12);
			this->groupBox3->Controls->Add(this->NPS_AmplitudeDC);
			this->groupBox3->Controls->Add(this->groupBox5);
			this->groupBox3->Location = System::Drawing::Point(243, 264);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(278, 233);
			this->groupBox3->TabIndex = 2;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"NPS";
			// 
			// NPS_DosaCoffTitle
			// 
			this->NPS_DosaCoffTitle->AutoSize = true;
			this->NPS_DosaCoffTitle->Location = System::Drawing::Point(7, 192);
			this->NPS_DosaCoffTitle->Name = L"NPS_DosaCoffTitle";
			this->NPS_DosaCoffTitle->Size = System::Drawing::Size(107, 13);
			this->NPS_DosaCoffTitle->TabIndex = 16;
			this->NPS_DosaCoffTitle->Text = L"����������� ����";
			// 
			// NPS_Coenficient_of_Dosa
			// 
			this->NPS_Coenficient_of_Dosa->Location = System::Drawing::Point(120, 192);
			this->NPS_Coenficient_of_Dosa->Name = L"NPS_Coenficient_of_Dosa";
			this->NPS_Coenficient_of_Dosa->Size = System::Drawing::Size(62, 20);
			this->NPS_Coenficient_of_Dosa->TabIndex = 15;
			this->NPS_Coenficient_of_Dosa->Text = L"2,00";
			// 
			// NPS_UnitDosa
			// 
			this->NPS_UnitDosa->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->NPS_UnitDosa->FormattingEnabled = true;
			this->NPS_UnitDosa->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"���", L"��", L"������" });
			this->NPS_UnitDosa->Location = System::Drawing::Point(118, 162);
			this->NPS_UnitDosa->Name = L"NPS_UnitDosa";
			this->NPS_UnitDosa->Size = System::Drawing::Size(62, 21);
			this->NPS_UnitDosa->TabIndex = 18;
			this->NPS_UnitDosa->SelectionChangeCommitted += gcnew System::EventHandler(this, &Compute_Parametrs::NPS_UnitDosa_SelectionChangeCommitted);
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Location = System::Drawing::Point(7, 170);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(109, 13);
			this->label24->TabIndex = 17;
			this->label24->Text = L"������� ���������";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(186, 137);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(27, 13);
			this->label20->TabIndex = 16;
			this->label20->Text = L"LSB";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(8, 141);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(80, 13);
			this->label12->TabIndex = 14;
			this->label12->Text = L"��������� DC";
			// 
			// NPS_AmplitudeDC
			// 
			this->NPS_AmplitudeDC->Location = System::Drawing::Point(118, 134);
			this->NPS_AmplitudeDC->Name = L"NPS_AmplitudeDC";
			this->NPS_AmplitudeDC->Size = System::Drawing::Size(62, 20);
			this->NPS_AmplitudeDC->TabIndex = 13;
			this->NPS_AmplitudeDC->Text = L"1000";
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->label19);
			this->groupBox5->Controls->Add(this->label18);
			this->groupBox5->Controls->Add(this->label11);
			this->groupBox5->Controls->Add(this->label10);
			this->groupBox5->Controls->Add(this->label9);
			this->groupBox5->Controls->Add(this->label8);
			this->groupBox5->Controls->Add(this->NPS_EndY);
			this->groupBox5->Controls->Add(this->NPS_BeginY);
			this->groupBox5->Controls->Add(this->NPS_EndX);
			this->groupBox5->Controls->Add(this->NPS_BeginX);
			this->groupBox5->Location = System::Drawing::Point(0, 22);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(273, 106);
			this->groupBox5->TabIndex = 4;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"������� �������";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(186, 69);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(25, 13);
			this->label19->TabIndex = 15;
			this->label19->Text = L"���";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(185, 43);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(25, 13);
			this->label18->TabIndex = 14;
			this->label18->Text = L"���";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(6, 65);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(38, 13);
			this->label11->TabIndex = 12;
			this->label11->Text = L"�����";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(6, 39);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(44, 13);
			this->label10->TabIndex = 11;
			this->label10->Text = L"������";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(142, 20);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(14, 13);
			this->label9->TabIndex = 10;
			this->label9->Text = L"Y";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(72, 20);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(14, 13);
			this->label8->TabIndex = 9;
			this->label8->Text = L"X";
			// 
			// NPS_EndY
			// 
			this->NPS_EndY->Location = System::Drawing::Point(118, 62);
			this->NPS_EndY->Name = L"NPS_EndY";
			this->NPS_EndY->Size = System::Drawing::Size(62, 20);
			this->NPS_EndY->TabIndex = 8;
			this->NPS_EndY->Text = L"1024";
			// 
			// NPS_BeginY
			// 
			this->NPS_BeginY->Location = System::Drawing::Point(118, 36);
			this->NPS_BeginY->Name = L"NPS_BeginY";
			this->NPS_BeginY->Size = System::Drawing::Size(62, 20);
			this->NPS_BeginY->TabIndex = 6;
			this->NPS_BeginY->Text = L"0";
			// 
			// NPS_EndX
			// 
			this->NPS_EndX->Location = System::Drawing::Point(50, 62);
			this->NPS_EndX->Name = L"NPS_EndX";
			this->NPS_EndX->Size = System::Drawing::Size(62, 20);
			this->NPS_EndX->TabIndex = 7;
			this->NPS_EndX->Text = L"1024";
			// 
			// NPS_BeginX
			// 
			this->NPS_BeginX->Location = System::Drawing::Point(50, 36);
			this->NPS_BeginX->Name = L"NPS_BeginX";
			this->NPS_BeginX->Size = System::Drawing::Size(62, 20);
			this->NPS_BeginX->TabIndex = 5;
			this->NPS_BeginX->Text = L"0";
			// 
			// SNR
			// 
			this->SNR->Location = System::Drawing::Point(123, 44);
			this->SNR->Name = L"SNR";
			this->SNR->Size = System::Drawing::Size(62, 20);
			this->SNR->TabIndex = 16;
			this->SNR->Text = L"30174";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(5, 23);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(89, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"���� � �������";
			// 
			// FramePath
			// 
			this->FramePath->Location = System::Drawing::Point(118, 20);
			this->FramePath->Name = L"FramePath";
			this->FramePath->ReadOnly = true;
			this->FramePath->Size = System::Drawing::Size(385, 20);
			this->FramePath->TabIndex = 1;
			this->FramePath->Text = L"Proba";
			this->FramePath->Click += gcnew System::EventHandler(this, &Compute_Parametrs::FramePath_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(10, 21);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(91, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"������ �������";
			// 
			// PixelSizeX
			// 
			this->PixelSizeX->Location = System::Drawing::Point(123, 18);
			this->PixelSizeX->Name = L"PixelSizeX";
			this->PixelSizeX->Size = System::Drawing::Size(62, 20);
			this->PixelSizeX->TabIndex = 3;
			this->PixelSizeX->Text = L"144";
			// 
			// PixelSizeY
			// 
			this->PixelSizeY->Location = System::Drawing::Point(213, 18);
			this->PixelSizeY->Name = L"PixelSizeY";
			this->PixelSizeY->Size = System::Drawing::Size(62, 20);
			this->PixelSizeY->TabIndex = 4;
			this->PixelSizeY->Text = L"144";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(103, 21);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(14, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"X";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(193, 21);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(14, 13);
			this->label4->TabIndex = 6;
			this->label4->Text = L"Y";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->groupBox4);
			this->groupBox1->Controls->Add(this->label21);
			this->groupBox1->Controls->Add(this->DeviceType);
			this->groupBox1->Controls->Add(this->FramePath);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(12, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(504, 246);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"����� ���������";
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->label25);
			this->groupBox4->Controls->Add(this->label14);
			this->groupBox4->Controls->Add(this->SNR);
			this->groupBox4->Controls->Add(this->groupBox9);
			this->groupBox4->Controls->Add(this->label23);
			this->groupBox4->Controls->Add(this->label2);
			this->groupBox4->Controls->Add(this->PixelSizeX);
			this->groupBox4->Controls->Add(this->Rotate);
			this->groupBox4->Controls->Add(this->PixelSizeY);
			this->groupBox4->Controls->Add(this->label22);
			this->groupBox4->Controls->Add(this->label3);
			this->groupBox4->Controls->Add(this->label4);
			this->groupBox4->Location = System::Drawing::Point(8, 76);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(490, 157);
			this->groupBox4->TabIndex = 17;
			this->groupBox4->TabStop = false;
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(193, 51);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(71, 13);
			this->label25->TabIndex = 17;
			this->label25->Text = L"��^2 * ����";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(282, 25);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(29, 13);
			this->label14->TabIndex = 7;
			this->label14->Text = L"���";
			// 
			// groupBox9
			// 
			this->groupBox9->Controls->Add(this->FlipHorizontal);
			this->groupBox9->Controls->Add(this->FlipVertical);
			this->groupBox9->Location = System::Drawing::Point(6, 107);
			this->groupBox9->Name = L"groupBox9";
			this->groupBox9->Size = System::Drawing::Size(276, 44);
			this->groupBox9->TabIndex = 15;
			this->groupBox9->TabStop = false;
			this->groupBox9->Text = L"��������� ������������";
			// 
			// FlipHorizontal
			// 
			this->FlipHorizontal->AutoSize = true;
			this->FlipHorizontal->Location = System::Drawing::Point(139, 19);
			this->FlipHorizontal->Name = L"FlipHorizontal";
			this->FlipHorizontal->Size = System::Drawing::Size(130, 17);
			this->FlipHorizontal->TabIndex = 1;
			this->FlipHorizontal->Text = L"�������������� ���";
			this->FlipHorizontal->UseVisualStyleBackColor = true;
			// 
			// FlipVertical
			// 
			this->FlipVertical->AutoSize = true;
			this->FlipVertical->Location = System::Drawing::Point(7, 20);
			this->FlipVertical->Name = L"FlipVertical";
			this->FlipVertical->Size = System::Drawing::Size(119, 17);
			this->FlipVertical->TabIndex = 0;
			this->FlipVertical->Text = L"������������ ���";
			this->FlipVertical->UseVisualStyleBackColor = true;
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(10, 51);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(50, 13);
			this->label23->TabIndex = 16;
			this->label23->Text = L"SNRin^2";
			// 
			// Rotate
			// 
			this->Rotate->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->Rotate->FormattingEnabled = true;
			this->Rotate->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"���", L"������ ������� �������", L"�� ������� �������" });
			this->Rotate->Location = System::Drawing::Point(123, 76);
			this->Rotate->Name = L"Rotate";
			this->Rotate->Size = System::Drawing::Size(159, 21);
			this->Rotate->TabIndex = 14;
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(10, 79);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(94, 13);
			this->label22->TabIndex = 13;
			this->label22->Text = L"�������  ������";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(5, 52);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(86, 13);
			this->label21->TabIndex = 12;
			this->label21->Text = L"��� ����������";
			// 
			// DeviceType
			// 
			this->DeviceType->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->DeviceType->FormattingEnabled = true;
			this->DeviceType->Items->AddRange(gcnew cli::array< System::Object^  >(4) {
				L"����� ������������", L"Mamo ", L"Proscann",
					L"Prograph"
			});
			this->DeviceType->Location = System::Drawing::Point(120, 49);
			this->DeviceType->Name = L"DeviceType";
			this->DeviceType->Size = System::Drawing::Size(170, 21);
			this->DeviceType->TabIndex = 11;
			// 
			// _Cancel_button
			// 
			this->_Cancel_button->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->_Cancel_button->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->_Cancel_button->Location = System::Drawing::Point(426, 546);
			this->_Cancel_button->Name = L"_Cancel_button";
			this->_Cancel_button->Size = System::Drawing::Size(75, 21);
			this->_Cancel_button->TabIndex = 4;
			this->_Cancel_button->Text = L"������";
			this->_Cancel_button->UseVisualStyleBackColor = true;
			this->_Cancel_button->Click += gcnew System::EventHandler(this, &Compute_Parametrs::_Cancel_button_Click);
			// 
			// _OK_button
			// 
			this->_OK_button->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->_OK_button->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->_OK_button->Location = System::Drawing::Point(345, 546);
			this->_OK_button->Name = L"_OK_button";
			this->_OK_button->Size = System::Drawing::Size(75, 21);
			this->_OK_button->TabIndex = 5;
			this->_OK_button->Text = L"��";
			this->_OK_button->UseVisualStyleBackColor = true;
			this->_OK_button->Click += gcnew System::EventHandler(this, &Compute_Parametrs::_OK_button_Click);
			// 
			// Compute_Parametrs
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->ClientSize = System::Drawing::Size(533, 572);
			this->Controls->Add(this->_OK_button);
			this->Controls->Add(this->_Cancel_button);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->DoubleBuffered = true;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"Compute_Parametrs";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"��������� �������";
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->groupBox9->ResumeLayout(false);
			this->groupBox9->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void _OK_button_Click(System::Object^  sender, System::EventArgs^  e) {
		
				WriteParametrs();
				PostMessageA(MainViewHandle, 0x10001,0, 0);
				this->Close();
			 }
private: System::Void NPS_UnitDosa_SelectionChangeCommitted(System::Object^  sender, System::EventArgs^  e) {


	if (NPS_UnitDosa->SelectedIndex == NPS_DosaUnitType_mAs)
	{
		NPS_Coenficient_of_Dosa->Visible = true;
		NPS_DosaCoffTitle->Visible = true;
		NPS_Coenficient_of_Dosa->Text = _DQE_Config->GetNPS_CoenficinentDosa_mAs().ToString();
	}
	if (NPS_UnitDosa->SelectedIndex == NPS_DosaUnitType_mA)
	{
		NPS_Coenficient_of_Dosa->Visible = true;
		NPS_DosaCoffTitle->Visible = true;
		NPS_Coenficient_of_Dosa->Text = _DQE_Config->GetNPS_CoenficinentDosa_mA().ToString();
	}

	if (NPS_UnitDosa->SelectedIndex == NPS_DosaUnitType_mkGr)
	{
		NPS_Coenficient_of_Dosa->Visible = false;
		NPS_DosaCoffTitle->Visible = false;
	}


}
private: System::Void FramePath_Click(System::Object^  sender, System::EventArgs^  e) {
	if (PathFrames->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		if (PathFrames->SelectedPath->Length>0 )
		{
			Path = PathFrames->SelectedPath;
			FramePath->Text = Path;
		}
		
	}

}
private: System::Void _Cancel_button_Click(System::Object^  sender, System::EventArgs^  e) {
	this->Close();
}
};
}
