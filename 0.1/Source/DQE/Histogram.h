#pragma once
#define NOMINMAX
#include <stdint.h>
#include <Windows.h>
#include <omp.h>
#include "Posix_Rect.h"
#include <algorithm>
#include <functional>
#ifndef  _HISTOGRAM_
#define  _HISTOGRAM_

/*************************************************************************************************
*                 ������ ��� ������ � ������������ 
*                 ���������� �.�(molotatliev@xprom.ru)
*                 Initial version:22.06.18
*
*
*
*
***************************************************************************************************/

class Histogram
{
public:
    Histogram(void);
    ~Histogram(void);
	void SetMaxBins(size_t BinsCount);
    int Compute(uint16_t* Data,size_t Datacount);
	int Compute(uint16_t* Frame, size_t H, size_t W, Posix_Rect* ROI);
	void FindMax(uint16_t* Frame, size_t H, size_t W, Posix_Rect* ROI);
	void FindMin(uint16_t* Frame, size_t H, size_t W, Posix_Rect* ROI);

	size_t* GetHistogram(void);
	void Clear(void);
	size_t GetBinCount(void) { return  _BinsCount; }
	size_t operator[](size_t Position) {
		if (Position < _BinsCount) return _Histogram[Position];
	};
	size_t GetBinByIndex(size_t Position) { if (Position < _BinsCount) return _Histogram[Position]; };
void FindMax(uint16_t* Data, size_t Datacount);
void FindMin(uint16_t* Data, size_t Datacount);
void FindModa(void);
void FindMedain(uint16_t* Data, size_t Datacount);
size_t GetAreaOfInterval(size_t BeginBin, size_t EndBin);
size_t _Max;
size_t _Min;
size_t _Moda;
size_t _Median;
private:
size_t _BinsCount;
size_t* _Histogram;
uint16_t* _DataForMedian;
};
#endif
