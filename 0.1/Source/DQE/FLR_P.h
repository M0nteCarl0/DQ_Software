#pragma once

#include <fstream>
#include <stdint.h>
#include <Windows.h>
using namespace std;
#ifndef _FLR_POSIX_
#define _FLR_POSIX_
/*************************************************************************************************
*                 ������ ��� ������ � FLR ��������(POSIX)
*                 ���������� �.�(molotatliev@xprom.ru)
*                 Initial version:13.10.15
*
*
*
*
***************************************************************************************************/

typedef struct FLE_HEADER
{
uint16_t	_fType;
uint16_t	_fWidth;
uint16_t	_fHeight;
uint16_t	_fBpp;
uint16_t	_fBitsOffset;
}FLE_HEADER,&FLE_HEADER_R,*FLE_HEADER_P;

class   FLR_P
{
public:
	FLR_P(void);
	~FLR_P(void);
    bool Make(const char* Filename,const char* RawSource);
    bool Make2(const char* Filename,const char* RawSource,const char* Filename1,const char* RawSource1);
    bool Make(const char* Filename,uint16_t* Data,size_t Datasize);
    bool Make2(const char* Filename,uint16_t* Data,size_t Datasize,const char* Filename1,uint16_t* Data1,size_t Datasize1);
	bool Open(const char* Filename);
	uint16_t* GetDataBuffer(void) { return _ImageBuff; };
	size_t GetImageSize(void) { return _ImageSize;};
	bool SaveChanges(void);
	void Close(void);
	void GetImageResolution(uint64_t* Width, uint64_t* Height) { *Height = _Height; *Width = _Width; };
	
	void WriteData(uint16_t* Data,size_t Datasize);
	void ReadData(uint16_t* Data,size_t Datasize);
	void WriteHeader(FLE_HEADER_R Head);
	void ReadHeader(FLE_HEADER_R Head);
	bool IsOpen(void) { return _FIO.is_open(); }
	string Get_shortFileName(void) {
		return  _shortFileName;
	}
	size_t GetFileSize(void);
private:
	uint16_t _Resolution;
	uint64_t _Width;
	uint64_t _Height;
	FLE_HEADER _HeadF;
	size_t _Size;
	size_t _ImageSize;
	uint16_t* _ImageBuff;
	char* _ReadingBuffer;

	fstream  _FIO;
	string	 _FileName;
	string  _shortFileName;
	CRITICAL_SECTION _CriticalSection;
	template<typename T>  bool  Check(T* In,T* Out,size_t Count);
};
#endif
