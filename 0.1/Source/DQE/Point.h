#pragma once
#ifndef  _PONT
#define  _PONT
typedef struct Point
{
	size_t X;
	size_t Y;

}Point;
enum  DirectionMoveCursor
{

 DirectionMoveCursor_UP,
 DirectionMoveCursor_Down,
 DirectionMoveCursor_Right,
 DirectionMoveCursor_Left,
};
size_t GetLengthVectorByPoints(Point Begin, Point End);
size_t GetAngleBetweanVectors(Point Vector1Begin, Point Vector1End,Point Vector2Begin,Point Vector2End);
DirectionMoveCursor GetDirectionOfCursor(Point Begin, Point End);

#endif