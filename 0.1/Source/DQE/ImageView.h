#pragma once

#ifndef _ImageView_
#define _ImageView_

#include "LUT_Solver.h"
#include "Histogram.h"
#include "CPUInformation.h"
#include "ImageProcessor.h"
namespace DQE {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Drawing::Imaging;
	using namespace System::Runtime::InteropServices;
	/// <summary>
	/// ������ ��� Form1
	/// </summary>


	enum ComputeMode
	{
		ComputeMode_Statisctic = 0,
		ComputeMode_Optimization = 1,

	};

	public ref class ImageView: public System::Windows::Forms::Form
	{
	public:
	ImageProcessor* ImgProcessing;
	Pen^ DrawingPen;
	size_t _W;
	size_t _H;
    uint16_t* FrameBuffer;
	double RMS;
	double AVG;
	
    LUT_Solver* LUT;
	Histogram* Hist;
	CPUInformation* CPUMetrix;
	RectangleF^ RectangleImg;
	Bitmap^ FramePresentation;
	BitmapData^ PixelData;
	
	Posix_Rect* ROI;
	System::Drawing::Point PointBegin;

	System::Drawing::Rectangle AreaForCompute;
	System::Drawing::Rectangle ContantDrawinArea;
	Graphics^ g;
	bool MouseDown;
	size_t BeginRealX;
	size_t BeginRealY;
	int ComputeModeSelector;
	bool RegionReady;
	private: System::Windows::Forms::ToolStripStatusLabel^  MaxPixel;
	private: System::Windows::Forms::ToolStripStatusLabel^  MinPixel;
	private: System::Windows::Forms::ToolStripStatusLabel^  WhitePixelValue;
	private: System::Windows::Forms::ToolStripStatusLabel^  BlackPixelValue;

	private: System::Windows::Forms::ToolStripMenuItem^  ��������������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ���������������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripStatusLabel^  TimeLoadFile;
	private: System::Windows::Forms::ToolStripStatusLabel^  TimeCompute;
	private: System::Windows::Forms::ToolStripStatusLabel^  TimeDraw;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem2;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem4;
	private: System::Windows::Forms::ToolStripTextBox^  toolStripTextBox1;
	private: System::Windows::Forms::ToolStripStatusLabel^  TotalComputeTime;
	private: System::Windows::Forms::ToolStripStatusLabel^  MedianValueFrame;
	private: System::Windows::Forms::ContextMenuStrip^  FrameImageActions;
	private: System::Windows::Forms::ToolStripMenuItem^  DispersionAvergageCompute;
	private: System::Windows::Forms::ToolStripMenuItem^  OptimizationImage;




	private: System::Windows::Forms::ToolStripStatusLabel^  ResolutionImage;
    public: 
		
	private: System::Windows::Forms::PictureBox^  Frame;
	private: System::Windows::Forms::ToolStripStatusLabel^  StatisticInformation;
	private: System::Windows::Forms::ToolStripStatusLabel^  RectangleInformation;
	public:
		
	
/**************************************************************************************************************************/
          
		ImageView(void)
		{
			InitializeComponent();
			//this->ContextMenuStrip->Enabled = false;
			//this->ContextMenuStrip->Visible = false;
		  
			ComputeModeSelector = ComputeMode::ComputeMode_Optimization;
			OptimizationImage->Checked = true;
			DispersionAvergageCompute->Checked = false;
			RectangleInformation->Text = "";
			StatisticInformation->Text = "";
		    DrawingPen = gcnew Pen(Brushes::LightGoldenrodYellow);
			DrawingPen->Width = 3;
            LUT = new LUT_Solver();
            Hist = new Histogram();	
			CPUMetrix = new  CPUInformation();
			FrameBuffer = nullptr;
			ROI = new Posix_Rect();
			ImgProcessing = new ImageProcessor();
			omp_set_num_threads(CPUMetrix->GetLogicalCPUCount());
			omp_set_dynamic(CPUMetrix->GetLogicalCPUCount());
#pragma omp parallel num_threads(CPUMetrix->GetLogicalCPUCount())
#pragma omp_parallel schedule(static)
			menuStrip1->Visible = false;
			Threading::ThreadPool::SetMaxThreads(CPUMetrix->GetLogicalCPUCount(), CPUMetrix->GetLogicalCPUCount());
			Threading::ThreadPool::SetMinThreads(CPUMetrix->GetLogicalCPUCount(), CPUMetrix->GetLogicalCPUCount());

		}
/**************************************************************************************************************************/
	protected:
		System::Drawing::Rectangle ImageRectangle;
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
/**************************************************************************************************************************/
		virtual void WndProc(Message% m) override
	{

			Form::WndProc(m);
	}

/**************************************************************************************************************************/

		~ImageView()
		{

			if (ROI)
			{
				delete ROI;
			}

			if (ImgProcessing)
			{
				delete ImgProcessing;
			}


		

		    if (components)
		    {
		    	delete components;
            }

			if(CPUMetrix)
			{
				delete CPUMetrix;

			}

			

		    if(Hist)
            {
               delete Hist;

           }

           if(LUT)
           {
                delete LUT;
           }


        
            if(FrameBuffer)
            {
				FrameBuffer = nullptr;
				delete [] FrameBuffer;
            }


		}
/**************************************************************************************************************************/
    private: System::Windows::Forms::StatusStrip^  ImageViewstatusStrip;
    private: System::Windows::Forms::ToolStripStatusLabel^  PositionXFrame;
    private: System::Windows::Forms::ToolStripStatusLabel^  PositionYFrame;
    private: System::Windows::Forms::ToolStripStatusLabel^  AmplitudePixelFrame;
    private: System::Windows::Forms::ToolStripStatusLabel^  FileFormat;

	private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel1;

    private: System::Windows::Forms::MenuStrip^  menuStrip1;
    private: System::Windows::Forms::ToolStripMenuItem^  ����ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  �������ToolStripMenuItem;


    private: System::Windows::Forms::OpenFileDialog^  OpenImage;
    private: System::Windows::Forms::ToolStripMenuItem^  ������������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ��������������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  WhiteBalanceSetup;
private: System::ComponentModel::IContainer^  components;



	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->ImageViewstatusStrip = (gcnew System::Windows::Forms::StatusStrip());
			this->PositionXFrame = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->PositionYFrame = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->AmplitudePixelFrame = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->ResolutionImage = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->FileFormat = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->MaxPixel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->MinPixel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->MedianValueFrame = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->WhitePixelValue = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->BlackPixelValue = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->TimeLoadFile = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->TimeCompute = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->TimeDraw = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->TotalComputeTime = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->StatisticInformation = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->RectangleInformation = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->��������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->WhiteBalanceSetup = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->��������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripMenuItem4 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripTextBox1 = (gcnew System::Windows::Forms::ToolStripTextBox());
			this->FrameImageActions = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->DispersionAvergageCompute = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->OptimizationImage = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->OpenImage = (gcnew System::Windows::Forms::OpenFileDialog());
			this->Frame = (gcnew System::Windows::Forms::PictureBox());
			this->ImageViewstatusStrip->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			this->FrameImageActions->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Frame))->BeginInit();
			this->SuspendLayout();
			// 
			// ImageViewstatusStrip
			// 
			this->ImageViewstatusStrip->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(16) {
				this->PositionXFrame,
					this->PositionYFrame, this->AmplitudePixelFrame, this->ResolutionImage, this->FileFormat, this->MaxPixel, this->MinPixel, this->MedianValueFrame,
					this->WhitePixelValue, this->BlackPixelValue, this->TimeLoadFile, this->TimeCompute, this->TimeDraw, this->TotalComputeTime,
					this->StatisticInformation, this->RectangleInformation
			});
			this->ImageViewstatusStrip->Location = System::Drawing::Point(0, 563);
			this->ImageViewstatusStrip->Name = L"ImageViewstatusStrip";
			this->ImageViewstatusStrip->Size = System::Drawing::Size(1431, 22);
			this->ImageViewstatusStrip->TabIndex = 0;
			this->ImageViewstatusStrip->Text = L"statusStrip1";
			// 
			// PositionXFrame
			// 
			this->PositionXFrame->Name = L"PositionXFrame";
			this->PositionXFrame->Size = System::Drawing::Size(17, 17);
			this->PositionXFrame->Text = L"X:";
			// 
			// PositionYFrame
			// 
			this->PositionYFrame->Name = L"PositionYFrame";
			this->PositionYFrame->Size = System::Drawing::Size(17, 17);
			this->PositionYFrame->Text = L"Y:";
			// 
			// AmplitudePixelFrame
			// 
			this->AmplitudePixelFrame->Name = L"AmplitudePixelFrame";
			this->AmplitudePixelFrame->Size = System::Drawing::Size(18, 17);
			this->AmplitudePixelFrame->Text = L"A:";
			// 
			// ResolutionImage
			// 
			this->ResolutionImage->Name = L"ResolutionImage";
			this->ResolutionImage->Size = System::Drawing::Size(127, 17);
			this->ResolutionImage->Text = L"ImageResolution: X x X";
			// 
			// FileFormat
			// 
			this->FileFormat->Name = L"FileFormat";
			this->FileFormat->Size = System::Drawing::Size(63, 17);
			this->FileFormat->Text = L"FileFormat";
			this->FileFormat->Visible = false;
			// 
			// MaxPixel
			// 
			this->MaxPixel->Name = L"MaxPixel";
			this->MaxPixel->Size = System::Drawing::Size(56, 17);
			this->MaxPixel->Text = L"MaxPixel:";
			this->MaxPixel->Visible = false;
			// 
			// MinPixel
			// 
			this->MinPixel->Name = L"MinPixel";
			this->MinPixel->Size = System::Drawing::Size(55, 17);
			this->MinPixel->Text = L"MinPixel:";
			this->MinPixel->Visible = false;
			// 
			// MedianValueFrame
			// 
			this->MedianValueFrame->Name = L"MedianValueFrame";
			this->MedianValueFrame->Size = System::Drawing::Size(78, 17);
			this->MedianValueFrame->Text = L"Median value";
			this->MedianValueFrame->Visible = false;
			// 
			// WhitePixelValue
			// 
			this->WhitePixelValue->Name = L"WhitePixelValue";
			this->WhitePixelValue->Size = System::Drawing::Size(94, 17);
			this->WhitePixelValue->Text = L"����� �������:";
			this->WhitePixelValue->Visible = false;
			// 
			// BlackPixelValue
			// 
			this->BlackPixelValue->Name = L"BlackPixelValue";
			this->BlackPixelValue->Size = System::Drawing::Size(102, 17);
			this->BlackPixelValue->Text = L"������ �������:";
			this->BlackPixelValue->Visible = false;
			// 
			// TimeLoadFile
			// 
			this->TimeLoadFile->Name = L"TimeLoadFile";
			this->TimeLoadFile->Size = System::Drawing::Size(78, 17);
			this->TimeLoadFile->Text = L"TimeLoadFile";
			this->TimeLoadFile->Visible = false;
			// 
			// TimeCompute
			// 
			this->TimeCompute->Name = L"TimeCompute";
			this->TimeCompute->Size = System::Drawing::Size(84, 17);
			this->TimeCompute->Text = L"TimeCompute";
			this->TimeCompute->Visible = false;
			// 
			// TimeDraw
			// 
			this->TimeDraw->Name = L"TimeDraw";
			this->TimeDraw->Size = System::Drawing::Size(61, 17);
			this->TimeDraw->Text = L"TimeDraw";
			this->TimeDraw->Visible = false;
			// 
			// TotalComputeTime
			// 
			this->TotalComputeTime->Name = L"TotalComputeTime";
			this->TotalComputeTime->Size = System::Drawing::Size(113, 17);
			this->TotalComputeTime->Text = L"TotalOperationTime";
			this->TotalComputeTime->Visible = false;
			// 
			// StatisticInformation
			// 
			this->StatisticInformation->Name = L"StatisticInformation";
			this->StatisticInformation->Size = System::Drawing::Size(54, 17);
			this->StatisticInformation->Text = L"Statistica";
			// 
			// RectangleInformation
			// 
			this->RectangleInformation->Name = L"RectangleInformation";
			this->RectangleInformation->Size = System::Drawing::Size(122, 17);
			this->RectangleInformation->Text = L"RectangleInformation";
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->����ToolStripMenuItem,
					this->��������������ToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1431, 24);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// ����ToolStripMenuItem
			// 
			this->����ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->�������ToolStripMenuItem,
					this->������������ToolStripMenuItem
			});
			this->����ToolStripMenuItem->Name = L"����ToolStripMenuItem";
			this->����ToolStripMenuItem->Size = System::Drawing::Size(48, 20);
			this->����ToolStripMenuItem->Text = L"����";
			// 
			// �������ToolStripMenuItem
			// 
			this->�������ToolStripMenuItem->Name = L"�������ToolStripMenuItem";
			this->�������ToolStripMenuItem->Size = System::Drawing::Size(159, 22);
			this->�������ToolStripMenuItem->Text = L"�������";
			//this->�������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &ImageView::�������ToolStripMenuItem_Click);
			// 
			// ������������ToolStripMenuItem
			// 
			this->������������ToolStripMenuItem->Name = L"������������ToolStripMenuItem";
			this->������������ToolStripMenuItem->Size = System::Drawing::Size(159, 22);
			this->������������ToolStripMenuItem->Text = L"��������� ���..";
			// 
			// ��������������ToolStripMenuItem
			// 
			this->��������������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
				this->WhiteBalanceSetup,
					this->��������������ToolStripMenuItem, this->���������������ToolStripMenuItem, this->������ToolStripMenuItem
			});
			this->��������������ToolStripMenuItem->Name = L"��������������ToolStripMenuItem";
			this->��������������ToolStripMenuItem->Size = System::Drawing::Size(95, 20);
			this->��������������ToolStripMenuItem->Text = L"�����������";
			// 
			// WhiteBalanceSetup
			// 
			this->WhiteBalanceSetup->Name = L"WhiteBalanceSetup";
			this->WhiteBalanceSetup->Size = System::Drawing::Size(173, 22);
			this->WhiteBalanceSetup->Text = L"������ ������";
			this->WhiteBalanceSetup->Click += gcnew System::EventHandler(this, &ImageView::WhiteBalanceSetup_Click);
			// 
			// ��������������ToolStripMenuItem
			// 
			this->��������������ToolStripMenuItem->Name = L"��������������ToolStripMenuItem";
			this->��������������ToolStripMenuItem->Size = System::Drawing::Size(173, 22);
			this->��������������ToolStripMenuItem->Text = L"������ ��������";
			// 
			// ���������������ToolStripMenuItem
			// 
			this->���������������ToolStripMenuItem->Name = L"���������������ToolStripMenuItem";
			this->���������������ToolStripMenuItem->Size = System::Drawing::Size(173, 22);
			this->���������������ToolStripMenuItem->Text = L"������ ���������";
			// 
			// ������ToolStripMenuItem
			// 
			this->������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->toolStripMenuItem2,
					this->toolStripMenuItem4, this->toolStripTextBox1
			});
			this->������ToolStripMenuItem->Name = L"������ToolStripMenuItem";
			this->������ToolStripMenuItem->Size = System::Drawing::Size(173, 22);
			this->������ToolStripMenuItem->Text = L"�������";
			// 
			// toolStripMenuItem2
			// 
			this->toolStripMenuItem2->Name = L"toolStripMenuItem2";
			this->toolStripMenuItem2->Size = System::Drawing::Size(160, 22);
			this->toolStripMenuItem2->Text = L"++";
			// 
			// toolStripMenuItem4
			// 
			this->toolStripMenuItem4->Name = L"toolStripMenuItem4";
			this->toolStripMenuItem4->Size = System::Drawing::Size(160, 22);
			this->toolStripMenuItem4->Text = L"--";
			// 
			// toolStripTextBox1
			// 
			this->toolStripTextBox1->Name = L"toolStripTextBox1";
			this->toolStripTextBox1->Size = System::Drawing::Size(100, 23);
			this->toolStripTextBox1->Text = L"100%";
			// 
			// FrameImageActions
			// 
			this->FrameImageActions->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->DispersionAvergageCompute,
					this->OptimizationImage
			});
			this->FrameImageActions->Name = L"contextMenuStrip1";
			this->FrameImageActions->Size = System::Drawing::Size(186, 48);
			// 
			// DispersionAvergageCompute
			// 
			this->DispersionAvergageCompute->CheckOnClick = true;
			this->DispersionAvergageCompute->Name = L"DispersionAvergageCompute";
			this->DispersionAvergageCompute->Size = System::Drawing::Size(185, 22);
			this->DispersionAvergageCompute->Text = L"���������/�������";
			this->DispersionAvergageCompute->Click += gcnew System::EventHandler(this, &ImageView::DispersionAvergageCompute_Click);
			// 
			// OptimizationImage
			// 
			this->OptimizationImage->CheckOnClick = true;
			this->OptimizationImage->Name = L"OptimizationImage";
			this->OptimizationImage->Size = System::Drawing::Size(185, 22);
			this->OptimizationImage->Text = L"�����������";
			this->OptimizationImage->Click += gcnew System::EventHandler(this, &ImageView::OptimizationImage_Click);
			// 
			// OpenImage
			// 
			this->OpenImage->AddExtension = false;
			this->OpenImage->FileName = L"openFileDialog1";
			this->OpenImage->Filter = L"\"RP Frames|*. flr\".";
			// 
			// Frame
			// 
			this->Frame->Dock = System::Windows::Forms::DockStyle::Fill;
			this->Frame->Location = System::Drawing::Point(0, 24);
			this->Frame->Name = L"Frame";
			this->Frame->Size = System::Drawing::Size(1431, 539);
			this->Frame->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->Frame->TabIndex = 3;
			this->Frame->TabStop = false;
			this->Frame->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &ImageView::Frame_MouseClick);
			this->Frame->MouseDoubleClick += gcnew System::Windows::Forms::MouseEventHandler(this, &ImageView::Frame_MouseDoubleClick);
			this->Frame->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &ImageView::Frame_MouseDown);
			this->Frame->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &ImageView::Frame_MouseMove);
			this->Frame->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &ImageView::Frame_MouseUp);
			// 
			// ImageView
			// 
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Inherit;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->ClientSize = System::Drawing::Size(1431, 585);
			this->Controls->Add(this->Frame);
			this->Controls->Add(this->ImageViewstatusStrip);
			this->Controls->Add(this->menuStrip1);
			this->DoubleBuffered = true;
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"ImageView";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"X-Ray Image Viewer";
			this->ImageViewstatusStrip->ResumeLayout(false);
			this->ImageViewstatusStrip->PerformLayout();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->FrameImageActions->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Frame))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
/**************************************************************************************************************************/
    private: System::Void Frame_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	
		if (Frame->Image!=nullptr)
		{
			
			System::Drawing::Size ImageSize = FramePresentation->Size;
			float Ratio = Math::Min((float)Frame->ClientRectangle.Width /(float) ImageSize.Width, (float)Frame->ClientRectangle.Height /(float)ImageSize.Height);
			ImageRectangle.Width =(int)(ImageSize.Width * Ratio);
			ImageRectangle.Height =(int) (ImageSize.Height * Ratio);
			ImageRectangle.X = (Frame->ClientRectangle.Width - ImageRectangle.Width) / 2;
			ImageRectangle.Y = (Frame->ClientRectangle.Height - ImageRectangle.Height) / 2;
			
			if ((e->X >=  ImageRectangle.X   &&  e->X <= ImageRectangle.Right)   && (e->Y >=ImageRectangle.Y  && e->Y <= ImageRectangle.Bottom))
			{
				double ScaleFactorX =(double) Frame->Image->Width /(double)ImageRectangle.Width;
				double ScaleFactorY =(double) Frame->Image->Height / (double)ImageRectangle.Height;
				size_t PositionX = (e->X - ImageRectangle.X);
				size_t PositionY = (e->Y - ImageRectangle.Y);
				size_t RealPositionX = ((double)PositionX * ScaleFactorX);
				size_t RealPositionY =((double)PositionY * ScaleFactorY);
				
				PositionXFrame->Text = "W:(" + RealPositionX + ")";
				PositionYFrame->Text = "H:(" + RealPositionY + ")";

				if (RealPositionY  == FramePresentation->Height)
				{
					RealPositionY = FramePresentation->Height - 1;
				}

				if (RealPositionX == FramePresentation->Width)
				{
					RealPositionX = FramePresentation->Width - 1;
				}

				uint16_t RealPixValue = FrameBuffer[RealPositionY*FramePresentation->Width + RealPositionX];
				AmplitudePixelFrame->Text = "A:(" + RealPixValue + ")" ;
				if (MouseDown && e->Button == Windows::Forms::MouseButtons::Right )
				{
					
					if (e->X == PointBegin.X)
					{
						PointBegin.X = e->X + 10;

					}

					if ( e->Y ==  PointBegin.Y)
					{
						PointBegin.Y = e->Y + 10;
					}

					Frame->Refresh();
					if (e->X > PointBegin.X)
					{
						AreaForCompute.Width = e->X - PointBegin.X;
						AreaForCompute.X = PointBegin.X;

						ROI->right = (e->X- ImageRectangle.X)*ScaleFactorX;
						ROI->left = ScaleFactorX *  (PointBegin.X- ImageRectangle.X);
					}
					else
					{
						AreaForCompute.Width = PointBegin.X - e->X;
						AreaForCompute.X = e->X;
						ROI->right = (PointBegin.X -  ImageRectangle.X)*ScaleFactorX;
						ROI->left = ScaleFactorX * (e->X- ImageRectangle.X);
						
					}
						
					
						
					if (e->Y > PointBegin.Y)
					{
						AreaForCompute.Height = e->Y - PointBegin.Y;
						AreaForCompute.Y = PointBegin.Y;
						ROI->top =  (PointBegin.Y - ImageRectangle.Y)*ScaleFactorY;
						ROI->bottom = (e->Y - ImageRectangle.Y)* ScaleFactorY;
					
					}
					else
					{
						AreaForCompute.Height = PointBegin.Y - e->Y;
						AreaForCompute.Y = e->Y;
						ROI->top = (e->Y - ImageRectangle.Y)*ScaleFactorY;
						ROI->bottom = (PointBegin.Y - ImageRectangle.Y) * ScaleFactorY;
						

					}
					RectangleInformation->Text = "S:(" + ROI->GetArea() +"px" + ")" + " H: (" + ROI->GetHeight() + "px" + ")" + " W: (" + ROI->GetWidth() +  "px)";
					Frame->CreateGraphics()->DrawRectangle(DrawingPen, AreaForCompute);
				
				
				}
				
			}
			else
			{
				PositionXFrame->Text = "W:(" + 0 + ")";
				PositionYFrame->Text = "H:(" + 0 + ")";
				AmplitudePixelFrame->Text = "A:(" + 0 + ")";
			}

			
		}
		if (e->Button == System::Windows::Forms::MouseButtons::Right)
		{
			//Rectangle->Y = e->Y;
			//Rectangle->X = e->X;
		
		}
		
		
             }
/**************************************************************************************************************************/
void BinariesImage(Bitmap^ bmp,uint16_t* ImageSource, size_t W, size_t H, LUT_Solver* Solver)
			 {
				 byte PixelValue;
				 System::Drawing::Rectangle Rect; //(0, 0, bmp->Width, bmp->Height);
				 Rect.X = 0;
				 Rect.Y = 0;
				 Rect.Height = bmp->Height;
				 Rect.Width = bmp->Width;
				 size_t TotalPixels = W*H;
				 size_t RawPixelCounter = 0;
				 size_t j1;
				 byte* BackBuffer;
				 byte bitsPerPixel = 24;
				
				 PixelData = bmp->LockBits(Rect, System::Drawing::Imaging::ImageLockMode::ReadWrite,
					 bmp->PixelFormat);


				 if (W == H)
				 {


					 byte* Row = (byte*)PixelData->Scan0.ToPointer();

//#pragma omp parallel num_threads(CPUMetrix->GetLogicalCPUCount())
//#pragma omp_parallel schedule(dinamic)			
//#pragma omp_parallel for shared(PixelRaw, ImageSource,Solver,PixelValue) private(TotalPixels,RawPixelCounter)
					 {
						 for (size_t j = 0; j < TotalPixels; j++)
						 {
							 PixelValue = (byte)Solver->GetPixelFromLUT_Fast(&ImageSource[j]);
							 Row[RawPixelCounter] = PixelValue;
							 Row[RawPixelCounter + 1] = PixelValue;
							 Row[RawPixelCounter + 2] = PixelValue;
							 RawPixelCounter += 3;
						 }
					 }
//#pragma omp barrier

				 }
				 else
				 {
					 byte bitsPerPixel = 24;

//#pragma omp_parallel for shared(PixelRaw, ImageSource,Solver,PixelValue) private(TotalPixels,RawPixelCounter)
					 for (size_t i = 0; i < TotalPixels; i++)
					 {

						
						size_t j = i%bmp->Width;
						size_t ii = i / bmp->Width;
						size_t Index = j + W*ii;
						byte* Pixel = (byte*)PixelData->Scan0.ToPointer() + ii * PixelData->Stride + j *3;//(byte*)PixelData->Scan0.ToPointer() + (ii*PixelData->Stride);  
						PixelValue = (byte)Solver->GetPixelFromLUT_Fast(&ImageSource[Index]);
						Pixel[0] = PixelValue;            // Red
						Pixel[1] = PixelValue;            // Green
						Pixel[2] = PixelValue;            // Blue


					 }
//#pragma omp barrier
				 }

				 bmp->UnlockBits(PixelData);
			 }

public: void SetTitle(String^ FormTitle)
{
	this->Text = FormTitle;

}

public: void SetBufferData(uint16_t* Data, size_t Ws, size_t Hs)
{

	_W = Ws;
	_H = Hs;
	ResolutionImage->Text = _W + "px" + " X " + _H + "px";
	if (FrameBuffer)
	{
		delete [] FrameBuffer;
		FrameBuffer = nullptr;
	}
	FrameBuffer = new uint16_t[_W*_H];
	memcpy(FrameBuffer, Data, Ws*Hs * sizeof(uint16_t));


}

public: void SetContastRegion(System::Drawing::Rectangle _ContantDrawinArea, bool  _RegionReady)
{
	ContantDrawinArea = _ContantDrawinArea;
	RegionReady = _RegionReady;
}

public: void BinariesFrame(void)
{

	Hist->SetMaxBins(65536);
	
	Hist->Compute(FrameBuffer, _W*_H);
	LUT->ComputeThershold(Hist, _W, _H);
	
	
	
	FramePresentation = gcnew Bitmap(_W, _H, PixelFormat::Format24bppRgb);
	BinariesImage(FramePresentation, FrameBuffer, _W, _H, LUT);
	if (RegionReady)
	{
		Graphics^ g = Graphics::FromImage(FramePresentation);
		DrawingPen->Width = 5;
		DrawingPen->Color = System::Drawing::Color::DarkGray;
		g->DrawRectangle(DrawingPen, ContantDrawinArea);
	}
	Frame->Image = FramePresentation;


}


/**************************************************************************************************************************/
#if 0



	private: System::Void �������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		
		OpenImage->Filter = "RP Frames (*.flr)|*.flr|RAW Frames (*.raw)|*.raw";
	
		if (OpenImage->ShowDialog() ==   System::Windows::Forms::DialogResult::OK)
		{
				if (FramePresentation)
				{
					delete FramePresentation;
				}

				if (FrameBuffer)
				{
					delete FrameBuffer;
				}

			    uint64_t W, H;
				TraceTotalTime->Start();
				if (OpenImage->FilterIndex == 1)
				{
					FileFormat->Text = "FLR";
					Trace->Start();
					FLR_Img->Open((char*)Marshal::StringToHGlobalAnsi(OpenImage->FileName).ToPointer());
					Trace->End();
					TimeLoadFile->Text = String::Format("����� �������� ������: {0:0.00} ��", Trace->GetEllapsedMiliseconds()); //"����� �������� ������: " +  Trace->GetEllapsedSeconds() + "S";
					FLR_Img->GetImageResolution(&W, &H);
					FrameBuffer = new uint16_t[W*H];
					memcpy(FrameBuffer, FLR_Img->GetDataBuffer(), sizeof(uint16_t) * W * H);
					FLR_Img->Close();

				}
				

				if (OpenImage->FilterIndex == 2)
				{
					FileFormat->Text = "RAW";
					Trace->Start();
					RAW_Img->Open((char*)Marshal::StringToHGlobalAnsi(OpenImage->FileName).ToPointer());
					Trace->End();
					TimeLoadFile->Text = String::Format("����� �������� ������: {0:0.00} ��", Trace->GetEllapsedMiliseconds());
					RAW_Img->GetImageResolution(&W, &H);
					FrameBuffer = new uint16_t[W*H];
					memcpy(FrameBuffer, RAW_Img->GetDataBuffer(), sizeof(uint16_t) * W * H);
					RAW_Img->Close();
				}


				Hist->SetMaxBins(65536);
				Trace->Start();
				Hist->Compute(FrameBuffer, W*H);
				LUT->ComputeThershold(Hist,W,H);
				Trace->End();
				_W = W;
				_H= H;

				TimeCompute->Text = String::Format("����� ������� LUT: {0:0.00} ��", Trace->GetEllapsedMiliseconds());// "����� ������� LUT: " + Trace->GetEllapsedSeconds() + "S";
				FramePresentation = gcnew Bitmap(W, H, PixelFormat::Format24bppRgb);
				Trace->Start();
				ROI->bottom = 1644 + 416;
				ROI->top = 1644 - 416;
				ROI->left = 1644 - 500;
				ROI->right = 1644 + 500;
				//ROI->SetRect(1644 - 500, 1644 - 416, 1644 + 500, 1644 + 416);
				//double Result = ImgProcessing->ComputeAvergageImage(FrameBuffer, W, H, ROI);
				//double RMS = ImgProcessing->ComputeRMSImage(FrameBuffer, W, H, ROI);
				//ImgProcessing->FlipImage(FrameBuffer, W, H, false,true);
				//ImgProcessing->RotateImage(FrameBuffer, W, H, -90);
				//Frame->Update();
				BinariesImage(FramePresentation, FrameBuffer, W, H, LUT);
				Trace->End();
				TimeDraw->Text = String::Format("����� ��������� ������: {0:0.00} ��", Trace->GetEllapsedMiliseconds());
				//Graphics^ g = Graphics::FromImage(FramePresentation);
				//Pen^ skyBluePen = gcnew Pen(Brushes::LightGoldenrodYellow);
				//skyBluePen->Width = 20;
			//	g->DrawRectangle(skyBluePen, (int)W / 4, (int)H / 4, W / 8, H / 8);
				Frame->Image =  FramePresentation;
				
				
				//Frame->Update();
				ResolutionImage->Text = "���������� ������: " + W + "px" + " X " + H + "px";
				WhitePixelValue->Text = "W:" + LUT->GetWhitePixelValue() + " LSB";
				BlackPixelValue->Text = "B:" + LUT->GetBlackPixeValue() + " LSB";
				MaxPixel->Text = "M�������:" +  Hist->_Max.ToString();
				MinPixel->Text = "M������: " + Hist->_Min.ToString();
				MedianValueFrame->Text = "�������:" + Hist->_Median.ToString();
				Hist->Clear();
				TraceTotalTime->End();
				TotalComputeTime->Text = String::Format("����� ����� ������������: {0:0.00} ��", TraceTotalTime->GetEllapsedMiliseconds());
		}
	}
#endif // 0
private: System::Void WhiteBalanceSetup_Click(System::Object^  sender, System::EventArgs^  e) {

#if 0


	if (!WhiteBalance->Visible)
		{
			if (WhiteBalance->IsDisposed)
			{
				WhiteBalance = gcnew WhiteBalanceView();
			}
			WhiteBalance->Show();
		}
#endif // 0
		 }
private: System::Void Frame_MouseDoubleClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	
	if (e->Button == Windows::Forms::MouseButtons::Right)
	{
		

	}
		 }
private: System::Void Frame_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	if (e->Button ==  Windows::Forms::MouseButtons::Middle)
	{
		FrameImageActions->Show(Frame, e->X, e->Y);
		
	}
}
private: System::Void Frame_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
	if (FramePresentation!=nullptr)
	{
		//BinariesImage(FramePresentation, FrameBuffer, _W, _H, LUT);
	//	Frame->Image = FramePresentation;
		//Frame->Image = FramePresentation;

	}
		

		 }
private: System::Void Frame_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {

	if (Frame->Image != nullptr)
	{
		if (e->Button == Windows::Forms::MouseButtons::Right)
		{

		//	Frame->Refresh();
			System::Drawing::Size ImageSize = FramePresentation->Size;
			float Ratio = Math::Min((float)Frame->ClientRectangle.Width / (float)ImageSize.Width, (float)Frame->ClientRectangle.Height / (float)ImageSize.Height);
			ImageRectangle.Width = (int)(ImageSize.Width * Ratio);
			ImageRectangle.Height = (int)(ImageSize.Height * Ratio);
			ImageRectangle.X = (Frame->ClientRectangle.Width - ImageRectangle.Width) / 2;
			ImageRectangle.Y = (Frame->ClientRectangle.Height - ImageRectangle.Height) / 2;

			if ((e->X >= ImageRectangle.X   &&  e->X <= ImageRectangle.Right) && (e->Y >= ImageRectangle.Y  && e->Y <= ImageRectangle.Bottom))
			{


				double ScaleFactorX = (double)Frame->Image->Width / (double)ImageRectangle.Width;
				double ScaleFactorY = (double)Frame->Image->Height / (double)ImageRectangle.Height;
				size_t PositionX = (e->X - ImageRectangle.X);
				size_t PositionY = (e->Y - ImageRectangle.Y);
				size_t RealPositionX = ((double)PositionX * ScaleFactorX);
				size_t RealPositionY = ((double)PositionY * ScaleFactorY);


				PointBegin.X = e->X;
				PointBegin.Y = e->Y;
				MouseDown = true;

				BeginRealX = RealPositionX;
				BeginRealY = RealPositionY;

			}

			}


		}

}
private: System::Void Frame_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {

	if (e->Button == Windows::Forms::MouseButtons::Right)
	{
		MouseDown = false;


		if (RegionReady)
		{
			Graphics^ g = Graphics::FromImage(FramePresentation);
			DrawingPen->Color = System::Drawing::Color::DarkGray;
			DrawingPen->Width = 5;
			g->DrawRectangle(DrawingPen, ContantDrawinArea);
		}


		switch (ComputeModeSelector)
		{
		case ComputeMode::ComputeMode_Optimization:
		{
			Hist->SetMaxBins(65536);
			Hist->Compute(FrameBuffer, _H, _W, ROI);
			LUT->ComputeThershold(Hist, ROI->GetHeight(), ROI->GetWidth());
			BinariesImage(FramePresentation, FrameBuffer, _W, _H, LUT);
			Frame->Image = FramePresentation;
			
			WhitePixelValue->Text = "W:" + LUT->GetWhitePixelValue() + " LSB";
			BlackPixelValue->Text = "B:" + LUT->GetBlackPixeValue() + " LSB";
			MaxPixel->Text = "M�������:" + Hist->_Max.ToString();
			MinPixel->Text = "M������: " + Hist->_Min.ToString();
			Hist->Clear();
			break;
		}


		case ComputeMode::ComputeMode_Statisctic:
		{
			RMS =	ImgProcessing->ComputeRMSImage(FrameBuffer, _W, _H, ROI);
			AVG = 	ImgProcessing->ComputeAvergageImage(FrameBuffer, _W, _H, ROI);
			Frame->Image = FramePresentation;
			StatisticInformation->Text = String::Format("RMS:({0:0.##}) AVG:({1:0.##})", RMS, AVG);
			break;
		}

		default:
			break;
		}







	}


}
private: System::Void DispersionAvergageCompute_Click(System::Object^  sender, System::EventArgs^  e) {

	OptimizationImage->Checked = false;
	DispersionAvergageCompute->Checked = true;
	ComputeModeSelector = ComputeMode::ComputeMode_Statisctic;
	DrawingPen->Color = Color::Red;

}
private: System::Void OptimizationImage_Click(System::Object^  sender, System::EventArgs^  e) {
	OptimizationImage->Checked = true;
	DispersionAvergageCompute->Checked = false;
	ComputeModeSelector = ComputeMode::ComputeMode_Optimization;
	DrawingPen->Color = Color::Gold;
}
};
}
#endif // !_ImageView_