#pragma once
#include "Histogram.h"
#include <omp.h>
/*************************************************************************************************
*                 ������ ��� ������ � LUT 
*                 ���������� �.�(molotatliev@xprom.ru)
*                 Initial version:22.06.18
*
*
*
*
***************************************************************************************************/
class LUT_Solver
{
public:
    LUT_Solver(void);
    ~LUT_Solver(void);
	void ComputeThershold(Histogram * Histogram,size_t ImageH, size_t ImageW, float PercentsOfAreaWhite = 0.5, float PercentsOfAreaBlack = 0.5,bool UseFastLUT = true);
	short GetPixelFromLUT(uint16_t* RawPixel);
	inline short GetPixelFromLUT_Fast(uint16_t* RawPixel) { return _LUT[*RawPixel]; };
	size_t GetBlackPixeValue(void) { return _BlackPixel;};
	size_t GetWhitePixelValue(void){ return _WhitePixel;};
	short GetRealPixelValue(uint8_t Color);

	float GetPercentsAreaOfThersholdWhite(void) { return _PercentsAreaOfThersholdWhite; };
	float GetPercentsAreaOfThersholdBlack(void) { return _PercentsAreaOfThersholdBlack; };
	size_t GetAreaOfImage(void) { return _AreaOfImage; };
	size_t GetAreaOfThersholdWhite(void) { return _AreaOfThersholdWhite; };
	size_t GetAreaOfThersholdBlack(void) { return _AreaOfThersholdBlack; };
	size_t GetBitsPerPixel(void) {return _BitsPerPixel; };
	size_t GetMaxValuePalitra(void){return  _MaxValuePalitra;};
	void SetBitsPerPixel(size_t  BitsPerPixel) { _BitsPerPixel = BitsPerPixel; };
	void SetMaxValuePalitra(size_t  MaxValuePalitra){ _MaxValuePalitra = MaxValuePalitra;};
private:
size_t _BlackPixel;
size_t _WhitePixel;
float _PercentsAreaOfThersholdWhite;
float _PercentsAreaOfThersholdBlack;
size_t _GammaBitsPerPixel;
size_t _AreaOfImage;
size_t _AreaOfThersholdWhite;
size_t _AreaOfThersholdBlack;
size_t _BitsPerPixel;
size_t _MaxValuePalitra;
uint16_t* _LUT;
float _ScaleFactor;
};

