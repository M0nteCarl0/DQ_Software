#include "ConfigParcer.h"


ConfigParcer::ConfigParcer(void)
{  
     _IsConfigFileOpen = false;

}
/**************************************************************************************************************/
ConfigParcer::ConfigParcer(const char* FileName)
{
  _IsConfigFileOpen = false;
    Open(FileName);	
}
/**************************************************************************************************************/
bool ConfigParcer::Open(const char* Filename)
{
	if (_File.is_open())
	{
		_File.close();

	}

    _IsConfigFileOpen = false;
    _ConfigFileNamel = Filename;
    _File.open(Filename,std::ios::in);
    if(_File.is_open())
    {
     _IsConfigFileOpen = true;
    }

    return  _IsConfigFileOpen;
}
/**************************************************************************************************************/
void ConfigParcer::Close(void)
{
	if (_File.is_open())
	{
		_File.close();
	}
}
/**************************************************************************************************************/
ConfigParcer::~ConfigParcer(void)
{
	if (_File.is_open())
	{
		_File.close();
	}
}
	
/**************************************************************************************************************/
  void ConfigParcer::SetDelimetrToken(const char* DelimToken)
{
	_Delimetr = DelimToken;
}

/**************************************************************************************************************/
template <typename T> void  ConfigParcer::SetTemplateParam(const char* ParamName,T& Value)
{

	size_t Curent_Position;

	 _File.close(); 
     Open(_ConfigFileNamel.c_str());	  
	//_File.seekg(0, ios_base::beg);
     bool Seted = false;
			do
			{
			_File >> _Token;
			Curent_Position =static_cast<size_t>( _File.tellg());
	
	
			size_t _TID =static_cast<size_t>( _Token.find(_Delimetr));
	
			if(_TID!=-1)
			{


			string _ParamVa(_Token,0,_TID);
			string _ValueVa(_Token,_TID+1,_Token.length());
				
				if(_ParamVa == ParamName && Seted== false)
				{
					string DBG;
					_File.seekg(Curent_Position -_ValueVa.size() );
					_File << Value <<endl;
			
					 Seted = true;
				}
			}
			
			}while(!Seted || !_File.eof());

			


}

/**************************************************************************************************************/
template <typename T> void ConfigParcer::GetTemplateParamHex(const char* ParamName, T& Value  )
{
	
	      bool Founded = false;
		  size_t Curent_Position;
		  stringstream _SS;
		  _File.close(); 	 
          Open(_ConfigFileNamel.c_str());	
			do
			{
			_File >> _Token;
			Curent_Position = static_cast<size_t>( _File.tellg());
	
			size_t _TID =static_cast<size_t>( _Token.find(_Delimetr));

				string _ParamVa(_Token,0,_TID);
				string _ValueVa(_Token,_TID+1,_Token.length());
	
			if(_TID!=-1)
			{
          
				if(_ParamVa == ParamName)
				{
				
				_SS <<_ValueVa << std::hex;
				_SS >> Value;
				Founded = true;

				}
			}
			
			}while(!Founded || !_File.eof());
			  
			

}




/**************************************************************************************************************/
void ConfigParcer::SetIntParam(const char* ParamName,int& Value)
{
	     SetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::SetFloatParam(const char* ParamName,float& Value)
{
		SetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::SetDoubleParam(const char* ParamName,double& Value)
{
		SetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::SetBoolParam(const char* ParamName,bool& Value)
{
		SetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::GetIntParam(const char* ParamName,int& Value )
{
  GetTemplateParam(ParamName,Value);
}
/**************************************************************************************************************/
void ConfigParcer::GetFloatParam(const char* ParamName,float& Value )
{
	 GetTemplateParam(ParamName,Value);
}
/**************************************************************************************************************/
void ConfigParcer::GetDoubleParam(const char* ParamName,double& Value)
{
 GetTemplateParam(ParamName,Value);	
}
/**************************************************************************************************************/
void ConfigParcer::GetBoolParam(const char* ParamName,bool& Value)
{
	 GetTemplateParam(ParamName,Value);
}

/**************************************************************************************************************/
void ConfigParcer:: Getuint16_tParam(const char* ParamName,uint16_t& Value)
{
    GetTemplateParam(ParamName,Value);
}
/**************************************************************************************************************/
bool ConfigParcer:: GetIsConfigFileOpen(void)
{
return _IsConfigFileOpen;
}
/**************************************************************************************************************/
void ConfigParcer::GetIntParamHex(const char* ParamName,int& Value)
{

GetTemplateParamHex( ParamName,  Value );

}
