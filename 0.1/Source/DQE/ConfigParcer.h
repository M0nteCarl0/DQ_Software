#pragma once
#include <fstream>
#include <stdint.h>
#include <iostream>
#include <string>
#include <sstream>
using namespace std;
#ifndef _CONFIG_PARCE
#define _CONFIG_PARCE
//
//  autor: Alexander Molotaliev, molotaliev@roentgenprom.ru
//
//  date: 9.10.2015
//
class ConfigParcer
{
public:
	ConfigParcer(void);
	ConfigParcer(const char* FileName);
    bool Open(const char* Filename);
	void Close(void);
	~ConfigParcer(void);
void SetDelimetrToken(const char* DelimToken);
void SetIntParam(const char* ParamName,int& Value);
void SetFloatParam(const char* ParamName,float& Value);
void SetDoubleParam(const char* ParamName,double& Value);
void SetBoolParam(const char* ParamName,bool& Value);

void GetIntParam(const char* ParamName,int& Value);
void GetFloatParam(const char* ParamName,float& Value);
void GetDoubleParam(const char* ParamName,double& Value);
void GetBoolParam(const char* ParamName,bool& Value);
void Getuint16_tParam(const char* ParamName,uint16_t& Value);
void GetIntParamHex(const char* ParamName,int& Value);
template <typename T> void GetTemplateParam(const char* ParamName, T& Value);
bool GetIsConfigFileOpen(void);
fstream _File;

private:
template <typename T> void  SetTemplateParam(const char* ParamName,T& Value);

template <typename T> void GetTemplateParamHex(const char* ParamName, T& Value  );
	
	string _Token;
	string _Delimetr;
    bool _IsConfigFileOpen;
    string _ConfigFileNamel;

};

/**************************************************************************************************************/
 template <typename T>  inline void ConfigParcer::GetTemplateParam(const char* ParamName, T& Value)
{

	bool Founded = false;
	size_t Curent_Position = 0;
	stringstream _SS;
	//_File.close();
//	Open(_ConfigFileNamel.c_str());
	if (_File.is_open())
	{
		_File.seekg(0, ios_base::beg);
		do
		{
			_File >> _Token;
			Curent_Position = static_cast<size_t>(_File.tellg());

			size_t _TID = static_cast<size_t>(_Token.find(_Delimetr));

			string _ParamVa(_Token, 0, _TID);
			string _ValueVa(_Token, _TID + 1, _Token.length());

			if (_TID != -1)
			{

				if (_ParamVa == ParamName)
				{

					_SS << _ValueVa;
					_SS >> Value;
					break;

				}
			}

		} while (!_File.eof());
	}
//	_File.close();

}

#endif
