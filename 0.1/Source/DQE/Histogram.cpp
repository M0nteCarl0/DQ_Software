#include "StdAfx.h"
#include "Histogram.h"
#include <algorithm>


/********************************************************************/
Histogram::Histogram(void)
{

	_Max = 0;
	_Min  = 65536;
 _BinsCount = 0;
 _Median = 0;
 _Histogram = nullptr;


}

/********************************************************************/
Histogram::~Histogram(void)
{
 _BinsCount = 0;
Clear();

}
/********************************************************************/
int Histogram::Compute(uint16_t* Data, size_t Datacount)
{
  int Err = 0;

#pragma omp_parallel  for  shared(Data,_Histogram) private(Datacount)
  {
	  for (size_t i = 0; i < Datacount; i++)
	  {
		  _Histogram[Data[i]]++;
	  }
  }
#pragma omp barrier
	FindMax(Data,Datacount);
	FindMin(Data,Datacount);
	//FindMedain(Data, Datacount);
	//FindModa();
    return Err;
}
/********************************************************************/
int Histogram::Compute(uint16_t * Frame, size_t H, size_t W, Posix_Rect * ROI)
{
	memset(_Histogram, 0, _BinsCount * sizeof(size_t));
	if (ROI != nullptr)
	{
		uint16_t CheckVal = 0;
		for (size_t i = ROI->GetTop(); i < ROI->GetBotton(); i++)//������ 
		{
			for (size_t j = ROI->GetLeft(); j < ROI->GetRight(); j++)//�������
			{
				CheckVal = Frame[i*W + j];
				_Histogram[Frame[i*W + j]]++;
			}
		}

		FindMax(Frame, H, W, ROI);
		FindMin(Frame, H, W, ROI);
		size_t MaxRegion = _Max;
		FindMax(Frame, W*H);
		size_t  MaxImage = _Max;
		_Max = std::max(MaxRegion, MaxImage);

	}




	return 0;
}

void Histogram::FindMax(uint16_t * Frame, size_t H, size_t W, Posix_Rect * ROI)
{
	if (ROI != nullptr)
	{
		_Max = 0;
		for (size_t i = ROI->GetTop(); i < ROI->GetBotton(); i++)//������ 
		{
			for (size_t j = ROI->GetLeft(); j < ROI->GetRight(); j++)//�������
			{
				if(Frame[i*W + j] > _Max)
				{
					_Max = Frame[i*W + j];

				}
			}
		}
	}
}
/********************************************************************/
void Histogram::FindMin(uint16_t * Frame, size_t H, size_t W, Posix_Rect * ROI)
{
	if (ROI != nullptr)
	{
	   _Min = 65536;
		for (size_t i = ROI->GetTop(); i < ROI->GetBotton(); i++)//������ 
		{
			for (size_t j = ROI->GetLeft(); j < ROI->GetRight(); j++)//�������
			{
				if (Frame[i*W + j] < _Min)
				{
					_Min = Frame[i*W + j];

				}

			}
		}
	}
}
/********************************************************************/
void Histogram::SetMaxBins(size_t BinsCount) {
	_BinsCount = BinsCount; _Histogram = new size_t[BinsCount];
	memset(_Histogram, 0, BinsCount * sizeof(size_t));

};

/********************************************************************/
size_t* Histogram::GetHistogram(void) { 
	return _Histogram;
};
/********************************************************************/
void Histogram::Clear(void) {
	_Max = 0;
	_Min  = 65536;
	if (_Histogram) delete[] _Histogram ; _Histogram = nullptr;
}
/********************************************************************/
inline void Histogram::FindMax(uint16_t* Data, size_t Datacount)
{
#pragma omp_parallel for  shared(Data,_Max) private(Datacount)
	{
		for (size_t i = 0; i < Datacount; i++)
		{
			if (Data[i] > _Max)
			{
				_Max = Data[i];
			}
		}
	}
#pragma omp barrier
}
/********************************************************************/
inline void Histogram::FindMin(uint16_t* Data, size_t Datacount)
{
#pragma omp_parallel  for  shared(Data,_Max) private(Datacount)
	{
		for (size_t i = 0; i < Datacount; i++)
		{
			if (Data[i] < _Min)
			{
				_Min = Data[i];
			}
		}
	}
#pragma omp barrier
}
/********************************************************************/
inline void Histogram::FindModa(void)
{
}
/********************************************************************/
void Histogram::FindMedain(uint16_t* Data, size_t Datacount)
{
	if (_Histogram)
	{
		_DataForMedian = new uint16_t[Datacount];

		memcpy(_DataForMedian, Data, sizeof(uint16_t)*Datacount);
		std::sort(_DataForMedian,_DataForMedian+Datacount);
	
		if (Datacount % 2 == 0)
		{
			size_t Id = Datacount / 2;
			_Median = _DataForMedian[Id];
		}
		else
		{
			size_t Begin = Datacount / 2;
			_Median = (_DataForMedian[Begin] + _DataForMedian[Begin+1])/2;
		};

		delete _DataForMedian;
		
	}
}
/********************************************************************/
size_t Histogram::GetAreaOfInterval(size_t BeginBin, size_t EndBin)
{
	size_t Area = 0;
	if (_Histogram)
	{
#pragma omp_parallel  for  shared(_Histogram,Area) private(BeginBin,EndBin)
		{
			for (size_t i = BeginBin; i < EndBin; i++)
			{
				Area += _Histogram[i];

			}
		}
#pragma omp barrier
	}

	return Area;
}

